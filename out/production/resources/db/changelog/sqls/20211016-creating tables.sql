CREATE SCHEMA mcm;

CREATE TABLE public.snapshot_transaction
(
    id                     bigint NOT NULL,
    description            varchar(255),
    execution_date_final   timestamp,
    execution_date_initial timestamp,
    final_date             timestamp,
    initial_date           timestamp,
    status_job             varchar(255),
    service_performed      varchar(255),

    CONSTRAINT snapshot_transaction_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE public.snapshot_transaction_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE public.snapshot_transaction_id_seq OWNER TO postgres;
ALTER TABLE public.snapshot_transaction
    ALTER COLUMN id SET DEFAULT nextval('snapshot_transaction_id_seq'::regclass);

ALTER TABLE public.snapshot_transaction
    OWNER to postgres;

CREATE TABLE mcm.offer_cm51
(
    id            BIGINT NOT NULL,
    offer_id      VARCHAR(255),
    product_sku   VARCHAR(255),
    price         DECIMAL,
    quantity      BIGINT,
    description   TEXT,
    shop_id       BIGINT,
    shop_name     VARCHAR(255),
    active        BOOLEAN,
    updated_since TIMESTAMP WITHOUT TIME ZONE,

    CONSTRAINT pk_offer_cm51 PRIMARY KEY (id)
);

CREATE SEQUENCE mcm.offer_cm51_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE mcm.offer_cm51_id_seq OWNER TO postgres;
ALTER TABLE mcm.offer_cm51
    ALTER COLUMN id SET DEFAULT nextval('mcm.offer_cm51_id_seq'::regclass);

ALTER TABLE mcm.offer_cm51
    OWNER to postgres;
