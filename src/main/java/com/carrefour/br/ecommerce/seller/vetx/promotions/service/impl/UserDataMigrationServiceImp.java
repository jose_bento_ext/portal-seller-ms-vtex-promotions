package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.dto.UserDataDTO;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.UserDataMigrationRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.UserDataMigrationService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserDataMigrationServiceImp implements UserDataMigrationService {

    @Autowired
    private UserDataMigrationRepository userDataMigrationRepository;

    @Override
    public Optional<UserDataDTO> findByEmail(String email) {
        return Optional.ofNullable(userDataMigrationRepository.findByEmail(email));
    }
}
