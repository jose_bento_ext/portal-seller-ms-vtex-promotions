package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.ProductResponse;

import java.util.List;

public interface ProductService {
    void fillAndSaveEntity(List<ProductResponse> productResponses, Promotion promotion);
}

