package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.AffiliateResponse;

import java.util.List;

public interface AffiliateService {
    void fillAndSaveEntity(List<AffiliateResponse> affiliateResponseList, Promotion promotion);
}
