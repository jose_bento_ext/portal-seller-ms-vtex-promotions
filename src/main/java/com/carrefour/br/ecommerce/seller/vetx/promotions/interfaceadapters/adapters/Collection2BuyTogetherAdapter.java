package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Collection2BuyTogether;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class Collection2BuyTogetherAdapter {

    public static Collection2BuyTogether convertCollection2BuyTogetherToEntity(String collectionString, Promotion promotion) {
        Collection2BuyTogether collection2BuyTogether = getEntity(new Collection2BuyTogether(), collectionString, promotion);
        collection2BuyTogether.setActive(Boolean.TRUE);
        collection2BuyTogether.setCreateDate(LocalDateTime.now());
        return collection2BuyTogether;
    }

    public static Collection2BuyTogether preencherCollection2BuyTogether(Collection2BuyTogether collection2BuyTogether, String collectionString, Promotion promotion) {
        Collection2BuyTogether newCollection2BuyTogether = getEntity(collection2BuyTogether, collectionString, promotion);
        newCollection2BuyTogether.setUpdateDate(LocalDateTime.now());
        return newCollection2BuyTogether;
    }

    private static Collection2BuyTogether getEntity(Collection2BuyTogether collection2BuyTogether, String collectionString, Promotion promotion) {
        collection2BuyTogether.setCollectionBuyTogether(Optional.ofNullable(collectionString).orElse(null));
        collection2BuyTogether.setPromotion(promotion);
        return collection2BuyTogether;
    }
}
