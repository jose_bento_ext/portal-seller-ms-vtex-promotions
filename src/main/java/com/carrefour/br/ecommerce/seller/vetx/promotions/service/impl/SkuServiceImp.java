package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Sku;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.SkuAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.SkuResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.SkuRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.SkuService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SkuServiceImp implements SkuService {

    @Autowired
    private SkuRepository skuRepository;

    private void saveSku(Sku brand) {
        skuRepository.saveAndFlush(brand);
    }

    @Override
    public void fillAndSaveEntity(List<SkuResponse> skuResponses, Promotion promotion) {
        if (Objects.nonNull(promotion)) {
            skuResponses.forEach(skuResponse -> {
                Optional<Sku> skuOptional = skuRepository.findByPromotionAndIdVtex(promotion, skuResponse.getId());
                Sku brand = skuOptional.isPresent() ? updateSku(skuOptional.get(), skuResponse, promotion)
                        : newSku(skuResponse, promotion);
                saveSku(brand);
            });
        }
    }

    private Sku newSku(SkuResponse skuResponse, Promotion promotion) {
        return SkuAdapter.convertSkuResponseToEntity(skuResponse, promotion);
    }

    private Sku updateSku(Sku sku, SkuResponse skuResponse, Promotion promotion) {
        return SkuAdapter.preencherSku(sku, skuResponse, promotion);
    }
}
