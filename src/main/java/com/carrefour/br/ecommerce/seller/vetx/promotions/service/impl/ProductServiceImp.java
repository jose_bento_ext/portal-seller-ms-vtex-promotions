package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Product;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.ProductAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.ProductResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.ProductRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductServiceImp implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    private void saveProduct(Product brand) {
        productRepository.saveAndFlush(brand);
    }

    @Override
    public void fillAndSaveEntity(List<ProductResponse> productResponses, Promotion promotion) {
        if (Objects.nonNull(promotion)) {
            productResponses.forEach(productResponse -> {
                Optional<Product> productOptional = productRepository.findByPromotionAndIdVtex(promotion, productResponse.getId());
                Product brand = productOptional.isPresent() ? updateProduct(productOptional.get(), productResponse, promotion)
                        : newProduct(productResponse, promotion);
                saveProduct(brand);
            });
        }
    }

    private Product newProduct(ProductResponse productResponse, Promotion promotion) {
        return ProductAdapter.convertProductResponseToEntity(productResponse, promotion);
    }

    private Product updateProduct(Product product, ProductResponse productResponse, Promotion promotion) {
        return ProductAdapter.preencherProduct(product, productResponse, promotion);
    }
}
