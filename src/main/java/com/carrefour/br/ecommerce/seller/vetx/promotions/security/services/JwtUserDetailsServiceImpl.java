package com.carrefour.br.ecommerce.seller.vetx.promotions.security.services;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.dto.UserDataDTO;
import com.carrefour.br.ecommerce.seller.vetx.promotions.security.JwtUserFactory;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.UserDataMigrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDataMigrationService userDataMigrationService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserDataDTO> userData = userDataMigrationService.findByEmail(username);

        if (userData.isPresent()) {
            return JwtUserFactory.create(userData.get());
        }

        throw new UsernameNotFoundException("Email não encontrado.");
    }

}
