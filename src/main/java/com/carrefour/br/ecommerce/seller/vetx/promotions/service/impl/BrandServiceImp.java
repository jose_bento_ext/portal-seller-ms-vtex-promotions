package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Brand;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.BrandAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.BrandResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.BrandRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.BrandService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class BrandServiceImp implements BrandService {

    @Autowired
    private BrandRepository brandRepository;

    private void saveStore(Brand brand) {
        brandRepository.saveAndFlush(brand);
    }

    @Override
    public void fillAndSaveEntity(List<BrandResponse> brandResponses, Promotion promotion) {
        if (Objects.nonNull(promotion)) {
            brandResponses.forEach(brandResponse -> {
                Optional<Brand> brandOptional = brandRepository.findByPromotionAndIdVtex(promotion, brandResponse.getId());
                Brand brand = brandOptional.isPresent() ? updateBrand(brandOptional.get(), brandResponse, promotion)
                        : newBrand(brandResponse, promotion);
                saveStore(brand);
            });
        }
    }

    private Brand newBrand(BrandResponse brandResponse, Promotion promotion) {
        return BrandAdapter.convertBrandResponseToEntity(brandResponse, promotion);
    }

    private Brand updateBrand(Brand brand, BrandResponse brandResponse, Promotion promotion) {
        return BrandAdapter.preencherBrand(brand, brandResponse, promotion);
    }
}
