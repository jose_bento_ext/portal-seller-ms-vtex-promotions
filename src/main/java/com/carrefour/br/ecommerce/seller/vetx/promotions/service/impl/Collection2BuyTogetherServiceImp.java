package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Collection2BuyTogether;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.Collection2BuyTogetherAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.Collection2BuyTogetherRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.Collection2BuyTogetherService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class Collection2BuyTogetherServiceImp implements Collection2BuyTogetherService {

    @Autowired
    private Collection2BuyTogetherRepository collection2BuyTogetherRepository;

    private void saveCollection2BuyTogether(Collection2BuyTogether store) {
        collection2BuyTogetherRepository.saveAndFlush(store);
    }

    @Override
    public void fillAndSaveEntity(List<String> collections, Promotion promotion) {
        String collectionString = collections.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<Collection2BuyTogether> collection2BuyTogetherOptional = collection2BuyTogetherRepository.findByPromotionAndCollectionBuyTogether(promotion, collectionString);
            if (collection2BuyTogetherOptional.isPresent()) {
                Collection2BuyTogether store = Collection2BuyTogetherAdapter.preencherCollection2BuyTogether(collection2BuyTogetherOptional.get(), collectionString, promotion);
                saveCollection2BuyTogether(store);
                return;
            }
        }
        Collection2BuyTogether newStore = Collection2BuyTogetherAdapter.convertCollection2BuyTogetherToEntity(collectionString, promotion);
        saveCollection2BuyTogether(newStore);
    }
}
