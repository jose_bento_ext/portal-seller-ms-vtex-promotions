package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Affiliate;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.AffiliateResponse;

import java.time.LocalDateTime;
import java.util.Optional;

public class AffiliateAdapter {

    public static Affiliate convertAffiliateResponseToEntity(AffiliateResponse affiliateResponse, Promotion promotion) {
        Affiliate affiliate = getEntity(new Affiliate(), affiliateResponse, promotion);
        affiliate.setActive(Boolean.TRUE);
        affiliate.setCreateDate(LocalDateTime.now());
        return affiliate;
    }

    public static Affiliate preencherAffiliate(Affiliate affiliate, AffiliateResponse affiliateResponse, Promotion promotion) {
        Affiliate newAffiliate = getEntity(affiliate, affiliateResponse, promotion);
        newAffiliate.setUpdateDate(LocalDateTime.now());
        return newAffiliate;
    }

    private static Affiliate getEntity(Affiliate affiliate, AffiliateResponse affiliateResponse, Promotion promotion) {
        affiliate.setIdVtex(Optional.ofNullable(affiliateResponse.getId()).orElse(null));
        affiliate.setName(Optional.ofNullable(affiliateResponse.getName()).orElse(null));
        affiliate.setPromotion(promotion);
        return affiliate;
    }
}
