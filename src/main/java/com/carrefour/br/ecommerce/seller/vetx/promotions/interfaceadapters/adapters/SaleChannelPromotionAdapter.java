package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.SaleChannelPromotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class SaleChannelPromotionAdapter {

    public static SaleChannelPromotion convertSaleChannelPromotionToEntity(String saleChannel, Promotion promotion) {
        SaleChannelPromotion saleChannelPromotion = getEntity(new SaleChannelPromotion(), saleChannel, promotion);
        saleChannelPromotion.setActive(Boolean.TRUE);
        saleChannelPromotion.setCreateDate(LocalDateTime.now());
        return saleChannelPromotion;
    }

    public static SaleChannelPromotion preencherSaleChannelPromotion(SaleChannelPromotion saleChannelPromotion, String saleChannel, Promotion promotion) {
        SaleChannelPromotion newSaleChannelPromotion = getEntity(saleChannelPromotion, saleChannel, promotion);
        newSaleChannelPromotion.setUpdateDate(LocalDateTime.now());
        return newSaleChannelPromotion;
    }

    private static SaleChannelPromotion getEntity(SaleChannelPromotion saleChannelPromotion, String saleChannel, Promotion promotion) {
        saleChannelPromotion.setSalesChannel(Optional.ofNullable(saleChannel).orElse(null));
        saleChannelPromotion.setPromotion(promotion);
        return saleChannelPromotion;
    }
}
