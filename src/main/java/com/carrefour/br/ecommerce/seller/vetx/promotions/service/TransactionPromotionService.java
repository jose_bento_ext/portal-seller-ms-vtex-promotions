package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.exception.exceptionhandler.BusinessRuleException;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PromotionResponse;

public interface TransactionPromotionService {
    PromotionResponse createPromotion(PromotionResponse promotionResponse) throws BusinessRuleException;

    PromotionResponse updatePromotion(PromotionResponse promotionResponse, Long idPromotion) throws BusinessRuleException;

}
