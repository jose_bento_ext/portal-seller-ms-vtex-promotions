package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Merchant;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.MerchantAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.MerchantRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.MerchantService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MerchantServiceImp implements MerchantService {

    @Autowired
    private MerchantRepository merchantRepository;

    private void saveStore(Merchant merchant) {
        merchantRepository.saveAndFlush(merchant);
    }

    @Override
    public void fillAndSaveEntity(List<String> merchants, Promotion promotion) {
        String merchantString = merchants.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<Merchant> merchantOptional = merchantRepository.findByPromotionAndMerchant(promotion, merchantString);
            if (merchantOptional.isPresent()) {
                Merchant merchant = MerchantAdapter.preencherMerchant(merchantOptional.get(), merchantString, promotion);
                saveStore(merchant);
                return;
            }
        }
        Merchant newMerchant = MerchantAdapter.convertMerchantToEntity(merchantString, promotion);
        saveStore(newMerchant);
    }
}
