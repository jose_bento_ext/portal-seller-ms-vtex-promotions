package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.util.List;

public interface StoreService {
    void fillAndSaveEntity(List<String> stores, Promotion promotion);
}
