package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.ZipCodeResponse;

import java.util.List;

public interface ZipCodeService {
    void fillAndSaveEntity(List<ZipCodeResponse> zipCodeResponses, Promotion promotion);
}
