package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Coupon;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class CouponAdapter {

    public static Coupon convertCouponToEntity(String storeString, Promotion promotion) {
        Coupon coupon = getEntity(new Coupon(), storeString, promotion);
        coupon.setActive(Boolean.TRUE);
        coupon.setCreateDate(LocalDateTime.now());
        return coupon;
    }

    public static Coupon preencherCoupon(Coupon coupon, String storeString, Promotion promotion) {
        Coupon newCoupon = getEntity(coupon, storeString, promotion);
        newCoupon.setUpdateDate(LocalDateTime.now());
        return newCoupon;
    }

    private static Coupon getEntity(Coupon coupon, String storeString, Promotion promotion) {
        coupon.setCoupon(Optional.ofNullable(storeString).orElse(null));
        coupon.setPromotion(promotion);
        return coupon;
    }
}
