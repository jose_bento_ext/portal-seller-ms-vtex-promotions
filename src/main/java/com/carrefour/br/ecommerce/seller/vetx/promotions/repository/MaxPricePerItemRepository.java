package com.carrefour.br.ecommerce.seller.vetx.promotions.repository;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.MaxPricePerItem;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MaxPricePerItemRepository extends JpaRepository<MaxPricePerItem, Long> {
    Optional<MaxPricePerItem> findByPromotionAndMaxPricePerItem(Promotion promotion, String maxPricePerItem);
}
