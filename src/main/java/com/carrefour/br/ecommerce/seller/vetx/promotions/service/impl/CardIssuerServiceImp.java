package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.CardIssuer;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.CardIssuerAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.CardIssuerRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.CardIssuerService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CardIssuerServiceImp implements CardIssuerService {

    @Autowired
    private CardIssuerRepository cardIssuerRepository;

    private void saveStore(CardIssuer store) {
        cardIssuerRepository.saveAndFlush(store);
    }

    @Override
    public void fillAndSaveEntity(List<String> cardIssuers, Promotion promotion) {
        String sardIssuerString = cardIssuers.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<CardIssuer> cardIssuerOptional = cardIssuerRepository.findByPromotionAndCardIssuer(promotion, sardIssuerString);
            if (cardIssuerOptional.isPresent()) {
                CardIssuer cardIssuer = CardIssuerAdapter.preencherRestrictionBin(cardIssuerOptional.get(), sardIssuerString, promotion);
                saveStore(cardIssuer);
                return;
            }
        }
        CardIssuer newCardIssuer = CardIssuerAdapter.convertRestrictionBinToEntity(sardIssuerString, promotion);
        saveStore(newCardIssuer);
    }
}
