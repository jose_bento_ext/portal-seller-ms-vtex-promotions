package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.PromotionAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PromotionResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.PromotionRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.*;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class PromotionServiceImp implements PromotionService {

    @Autowired
    private PromotionRepository promotionRepository;
    @Autowired
    private SkuGiftService skuGiftService;
    @Autowired
    private ActiveDaysOfWeekService activeDaysOfWeekService;
    @Autowired
    private MaxPricePerItemService maxPricePerItemService;
    @Autowired
    private SaleChannelPromotionService saleChannelPromotionService;
    @Autowired
    private MarketingtagService marketingtagService;
    @Autowired
    private PaymentMethodService paymentMethodService;
    @Autowired
    private StoreService storeService;
    @Autowired
    private CampaingPromotionService campaingPromotionService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private ProductService productService;
    @Autowired
    private SkuService skuService;
    @Autowired
    private Collection2BuyTogetherService collection2BuyTogetherService;
    @Autowired
    private CouponService couponService;
    @Autowired
    private CollectionService collectionService;
    @Autowired
    private RestrictionBinService restrictionBinService;
    @Autowired
    private CardIssuerService cardIssuerService;
    @Autowired
    private SlaPromotionService slaPromotionService;
    @Autowired
    private ZipCodeService zipCodeService;
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private ClusterExpressionService clusterExpressionService;
    @Autowired
    private PaymentRuleService paymentRuleService;
    @Autowired
    private GiftListTypeService giftListTypeService;
    @Autowired
    private ProductSpecificationService productSpecificationService;
    @Autowired
    private AffiliateService affiliateService;

    @Override
    public PromotionResponse createdEntity(PromotionResponse promotionResponse) {
        Promotion promotion = fillPromotion(promotionResponse);
        savePromotion(promotion);
        promotionResponse.setIdPromotion(promotion.getId());
        activeDaysOfWeekService.fillAndSaveEntity(promotionResponse.getActiveDaysOfWeek(), promotion);
        maxPricePerItemService.fillAndSaveEntity(promotionResponse.getMaxPricesPerItems(), promotion);
        saleChannelPromotionService.fillAndSaveEntity(promotionResponse.getIdsSalesChannel(), promotion);
        marketingtagService.fillAndSaveEntity(promotionResponse.getMarketingTags(), promotion);
        paymentMethodService.fillAndSaveEntity(promotionResponse.getPaymentsMethods(), promotion);
        storeService.fillAndSaveEntity(promotionResponse.getStores(), promotion);
        campaingPromotionService.fillAndSaveEntity(promotionResponse.getCampaigns(), promotion);
        categoryService.fillAndSaveEntity(promotionResponse.getCategories(), promotion);
        brandService.fillAndSaveEntity(promotionResponse.getBrands(), promotion);
        productService.fillAndSaveEntity(promotionResponse.getProducts(), promotion);
        skuService.fillAndSaveEntity(promotionResponse.getSkus(), promotion);
        collection2BuyTogetherService.fillAndSaveEntity(promotionResponse.getCollections2BuyTogether(), promotion);
        couponService.fillAndSaveEntity(promotionResponse.getCoupon(), promotion);
        collectionService.fillAndSaveEntity(promotionResponse.getCollections(), promotion);
        restrictionBinService.fillAndSaveEntity(promotionResponse.getRestrictionsBins(), promotion);
        cardIssuerService.fillAndSaveEntity(promotionResponse.getCardIssuers(), promotion);
        slaPromotionService.fillAndSaveEntity(promotionResponse.getSlasIds(), promotion);
        zipCodeService.fillAndSaveEntity(promotionResponse.getZipCodeRanges(), promotion);
        merchantService.fillAndSaveEntity(promotionResponse.getMerchants(), promotion);
        clusterExpressionService.fillAndSaveEntity(promotionResponse.getClusterExpressions(), promotion);
        paymentRuleService.fillAndSaveEntity(promotionResponse.getPaymentsRules(), promotion);
        giftListTypeService.fillAndSaveEntity(promotionResponse.getGiftListTypes(), promotion);
        productSpecificationService.fillAndSaveEntity(promotionResponse.getProductsSpecifications(), promotion);
        affiliateService.fillAndSaveEntity(promotionResponse.getAffiliates(), promotion);

        return promotionResponse;
    }

    @Override
    public PromotionResponse updateEntity(PromotionResponse promotionResponse, Long idPromotion) {
        return new PromotionResponse();
    }

    private Promotion fillPromotion(PromotionResponse promotionResponse) {
        Optional<Promotion> promotion = promotionRepository.findByIdCalculatorConfiguration(promotionResponse.getIdCalculatorConfiguration());
        if (promotion.isPresent()) {
            return PromotionAdapter.preencherPromotion(promotion.get(), promotionResponse, skuGiftService.fillAndSaveEntity(promotionResponse.getSkusGift(), promotion.get().getId()));
        }
        return PromotionAdapter.convertPromotionResponseToEntity(promotionResponse, skuGiftService.fillAndSaveEntity(promotionResponse.getSkusGift(), null));
    }

    public void savePromotion(Promotion promotion) {
        promotionRepository.saveAndFlush(promotion);
    }
}
