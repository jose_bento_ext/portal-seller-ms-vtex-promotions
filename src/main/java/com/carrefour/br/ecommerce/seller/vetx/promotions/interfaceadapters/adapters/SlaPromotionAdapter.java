package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.SlaPromotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class SlaPromotionAdapter {

    public static SlaPromotion convertSlaPromotionToEntity(String slaPromotionString, Promotion promotion) {
        SlaPromotion slaPromotion = getEntity(new SlaPromotion(), slaPromotionString, promotion);
        slaPromotion.setActive(Boolean.TRUE);
        slaPromotion.setCreateDate(LocalDateTime.now());
        return slaPromotion;
    }

    public static SlaPromotion preencherSlaPromotion(SlaPromotion slaPromotion, String slaPromotionString, Promotion promotion) {
        SlaPromotion newSlaPromotion = getEntity(slaPromotion, slaPromotionString, promotion);
        newSlaPromotion.setUpdateDate(LocalDateTime.now());
        return newSlaPromotion;
    }

    private static SlaPromotion getEntity(SlaPromotion slaPromotion, String slaPromotionString, Promotion promotion) {
        slaPromotion.setSlaId(Optional.ofNullable(slaPromotionString).orElse(null));
        slaPromotion.setPromotion(promotion);
        return slaPromotion;
    }
}
