package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.CategoryResponse;

import java.util.List;

public interface CategoryService {
    void fillAndSaveEntity(List<CategoryResponse> categoryResponses, Promotion promotion);
}
