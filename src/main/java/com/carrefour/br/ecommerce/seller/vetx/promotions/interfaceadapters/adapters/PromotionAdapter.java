package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.enums.EffectTypeEnum;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.enums.EnumConverter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.enums.TotalValueModeEnum;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.SkusGift;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PromotionResponse;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

public class PromotionAdapter {

    public static Promotion convertPromotionResponseToEntity(PromotionResponse promotionResponse, SkusGift skusGift) {
        Promotion promotion = getEntity(new Promotion(), promotionResponse, skusGift);
        promotion.setActive(Boolean.TRUE);
        promotion.setCreateDate(LocalDateTime.now());
        return promotion;
    }

    public static Promotion preencherPromotion(Promotion promotion, PromotionResponse promotionResponse, SkusGift skusGift) {
        Promotion newPromotion = getEntity(promotion, promotionResponse, skusGift);
        newPromotion.setUpdateDate(LocalDateTime.now());
        return newPromotion;
    }

    private static Promotion getEntity(Promotion promotion, PromotionResponse promotionResponse, SkusGift skusGift) {
        promotion.setIdCalculatorConfiguration(Optional.ofNullable(promotionResponse.getIdCalculatorConfiguration()).orElse(null));
        promotion.setNamePromotion(Optional.ofNullable(promotionResponse.getName()).orElse(null));
        promotion.setDescription(Optional.ofNullable(promotionResponse.getDescription()).orElse(null));
        promotion.setBeginDateUtc(Optional.ofNullable(promotionResponse.getBeginDate()).orElse(null));
        promotion.setEndDateUtc(Optional.ofNullable(promotionResponse.getEndDate()).orElse(null));
        promotion.setLastModified(Optional.ofNullable(promotionResponse.getLastModified()).orElse(null));
        promotion.setDaysAgoOfPurchases(Optional.ofNullable(promotionResponse.getDaysAgoOfPurchases()).orElse(null));
        promotion.setIsActive(Optional.ofNullable(promotionResponse.getIsActive()).orElse(null));
        promotion.setIsArchived(Optional.ofNullable(promotionResponse.getIsArchived()).orElse(null));
        promotion.setDisableDeal(Optional.ofNullable(promotionResponse.getDisableDeal()).orElse(null));
        promotion.setOffset(Optional.ofNullable(promotionResponse.getOffset()).orElse(null));
        promotion.setActivateGiftsMultiplier(Optional.ofNullable(promotionResponse.getActivateGiftsMultiplier()).orElse(null));
        promotion.setNewOffset(Optional.ofNullable(promotionResponse.getNewOffset()).orElse(null));
        promotion.setCumulative(Optional.ofNullable(promotionResponse.getCumulative()).orElse(null));
        EffectTypeEnum effectTypeEnum = Objects.nonNull(promotionResponse.getEffectType()) ? EnumConverter.lookup(EffectTypeEnum.class, promotionResponse.getEffectType()) : EffectTypeEnum.NO_DATA;
        promotion.setEffectType(effectTypeEnum);
        promotion.setDiscountType(Optional.ofNullable(promotionResponse.getDiscountType()).orElse(null));
        promotion.setNominalDiscountValue(Optional.ofNullable(promotionResponse.getNominalDiscountValue()).orElse(null));
        promotion.setAbsoluteShippingDiscountValue(Optional.ofNullable(promotionResponse.getAbsoluteShippingDiscountValue()).orElse(null));
        promotion.setNominalDiscountValue(Optional.ofNullable(promotionResponse.getNominalRewardValue()).orElse(null));
        promotion.setMaximumUnitPriceDiscount(Optional.ofNullable(promotionResponse.getMaximumUnitPriceDiscount()).orElse(null));
        promotion.setPercentualDiscountValue(Optional.ofNullable(promotionResponse.getPercentualDiscountValue()).orElse(null));
        promotion.setRebatePercentualDiscountValue(Optional.ofNullable(promotionResponse.getRebatePercentualDiscountValue()).orElse(null));
        promotion.setPercentualShippingDiscountValue(Optional.ofNullable(promotionResponse.getPercentualShippingDiscountValue()).orElse(null));
        promotion.setPercentualTax(Optional.ofNullable(promotionResponse.getPercentualTax()).orElse(null));
        promotion.setShippingPercentualTax(Optional.ofNullable(promotionResponse.getShippingPercentualTax()).orElse(null));
        promotion.setPercentualDiscountValueList1(Optional.ofNullable(promotionResponse.getPercentualDiscountValueList1()).orElse(null));
        promotion.setPercentualDiscountValueList2(Optional.ofNullable(promotionResponse.getPercentualDiscountValueList2()).orElse(null));
        promotion.setSkusGift(skusGift);
        promotion.setNominalRewardValue(Optional.ofNullable(promotionResponse.getNominalRewardValue()).orElse(null));
        promotion.setPercentualRewardValue(Optional.ofNullable(promotionResponse.getPercentualRewardValue()).orElse(null));
        promotion.setOrderStatusRewardValue(Optional.ofNullable(promotionResponse.getOrderStatusRewardValue()).orElse(null));
        promotion.setMaxNumberOfAffectedItems(Optional.ofNullable(promotionResponse.getMaxNumberOfAffectedItems()).orElse(null));
        promotion.setMaxNumberOfAffectedItemsGroupKey(Optional.ofNullable(promotionResponse.getMaxNumberOfAffectedItemsGroupKey()).orElse(null));
        promotion.setApplyToAllShippings(Optional.ofNullable(promotionResponse.getApplyToAllShippings()).orElse(null));
        promotion.setNominalTax(Optional.ofNullable(promotionResponse.getNominalTax()).orElse(null));
        promotion.setOrigin(Optional.ofNullable(promotionResponse.getOrigin()).orElse(null));
        promotion.setIdSeller(Optional.ofNullable(promotionResponse.getIdSeller()).orElse(null));
        promotion.setIdSellerIsInclusive(Optional.ofNullable(promotionResponse.getIdSellerIsInclusive()).orElse(null));
        promotion.setIdSellerIsInclusive(Optional.ofNullable(promotionResponse.getIdSellerIsInclusive()).orElse(null));
        promotion.setAreSalesChannelIdsExclusive(Optional.ofNullable(promotionResponse.getIdSellerIsInclusive()).orElse(null));
        promotion.setMarketingTagsAreNotInclusive(Optional.ofNullable(promotionResponse.getMarketingTagsAreNotInclusive()).orElse(null));
        promotion.setStoresAreInclusive(Optional.ofNullable(promotionResponse.getStoresAreInclusive()).orElse(null));
        promotion.setCategoriesAreInclusive(Optional.ofNullable(promotionResponse.getCategoriesAreInclusive()).orElse(null));
        promotion.setBrandsAreInclusive(Optional.ofNullable(promotionResponse.getBrandsAreInclusive()).orElse(null));
        promotion.setProductsAreInclusive(Optional.ofNullable(promotionResponse.getProductsAreInclusive()).orElse(null));
        promotion.setSkusAreInclusive(Optional.ofNullable(promotionResponse.getSkusAreInclusive()).orElse(null));
        promotion.setUtmSource(Optional.ofNullable(promotionResponse.getUtmSource()).orElse(null));
        promotion.setUtmCampaign(Optional.ofNullable(promotionResponse.getUtmCampaign()).orElse(null));
        promotion.setMinimumQuantityBuyTogether(Optional.ofNullable(promotionResponse.getMinimumQuantityBuyTogether()).orElse(null));
        promotion.setQuantityToAffectBuyTogether(Optional.ofNullable(promotionResponse.getQuantityToAffectBuyTogether()).orElse(null));
        promotion.setEnableBuyTogetherPerSku(Optional.ofNullable(promotionResponse.getEnableBuyTogetherPerSku()).orElse(null));
        promotion.setEnableBuyTogetherPerSku(Optional.ofNullable(promotionResponse.getEnableBuyTogetherPerSku()).orElse(null));
        promotion.setTotalValueFloor(Optional.ofNullable(promotionResponse.getTotalValueFloor()).orElse(null));
        promotion.setTotalValueCeling(Optional.ofNullable(promotionResponse.getTotalValueCeling()).orElse(null));
        promotion.setTotalValueIncludeAllItems(Optional.ofNullable(promotionResponse.getTotalValueIncludeAllItems()).orElse(null));
        TotalValueModeEnum totalValueModeEnum = Objects.nonNull(promotionResponse.getTotalValueMode()) ? EnumConverter.lookup(TotalValueModeEnum.class, promotionResponse.getTotalValueMode()) : TotalValueModeEnum.NO_TEMS;
        promotion.setTotalValueMode(totalValueModeEnum);
        promotion.setCollectionsIsInclusive(Optional.ofNullable(promotionResponse.getCollectionsIsInclusive()).orElse(null));
        promotion.setTotalValuePurchase(Optional.ofNullable(promotionResponse.getTotalValuePurchase()).orElse(null));
        promotion.setIsSlaSelected(Optional.ofNullable(promotionResponse.getIsSlaSelected()).orElse(null));
        promotion.setIsFirstBuy(Optional.ofNullable(promotionResponse.getIsFirstBuy()).orElse(null));
        promotion.setFirstBuyIsProfileOptimistic(Optional.ofNullable(promotionResponse.getFirstBuyIsProfileOptimistic()).orElse(null));
        promotion.setCompareListPriceAndPrice(Optional.ofNullable(promotionResponse.getCompareListPriceAndPrice()).orElse(null));
        promotion.setIsDifferentListPriceAndPrice(Optional.ofNullable(promotionResponse.getIsDifferentListPriceAndPrice()).orElse(null));
        promotion.setItemMaxPrice(Optional.ofNullable(promotionResponse.getItemMaxPrice()).orElse(null));
        promotion.setItemMinPrice(Optional.ofNullable(promotionResponse.getItemMinPrice()).orElse(null));
        promotion.setInstallment(Optional.ofNullable(promotionResponse.getInstallment()).orElse(null));
        promotion.setIsMinMaxInstallments(Optional.ofNullable(promotionResponse.getIsMinMaxInstallments()).orElse(null));
        promotion.setMinInstallment(Optional.ofNullable(promotionResponse.getMinInstallment()).orElse(null));
        promotion.setMaxInstallment(Optional.ofNullable(promotionResponse.getMaxInstallment()).orElse(null));
        promotion.setMaxUsage(Optional.ofNullable(promotionResponse.getMaxUsage()).orElse(null));
        promotion.setMaxUsagePerClient(Optional.ofNullable(promotionResponse.getMaxUsagePerClient()).orElse(null));
        promotion.setShouldDistributeDiscountAmongMatchedItems(Optional.ofNullable(promotionResponse.getShouldDistributeDiscountAmongMatchedItems()).orElse(null));
        promotion.setMultipleUsePerClient(Optional.ofNullable(promotionResponse.getMultipleUsePerClient()).orElse(null));
        promotion.setAccumulateWithManualPrice(Optional.ofNullable(promotionResponse.getAccumulateWithManualPrice()).orElse(null));
        promotion.setType(Optional.ofNullable(promotionResponse.getType()).orElse(null));
        promotion.setUseNewProgressiveAlgorithm(Optional.ofNullable(promotionResponse.getUseNewProgressiveAlgorithm()).orElse(null));
        promotion.setActive(Boolean.TRUE);
        return promotion;
    }
}
