package com.carrefour.br.ecommerce.seller.vetx.promotions.repository;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Product;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findByPromotionAndIdVtex(Promotion promotion, String campaign);
}
