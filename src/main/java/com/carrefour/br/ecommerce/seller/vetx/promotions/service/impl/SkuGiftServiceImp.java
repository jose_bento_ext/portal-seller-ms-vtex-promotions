package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.SkusGift;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.SkuGiftAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.SkuGiftResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.SkuGiftRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.SkuGiftService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SkuGiftServiceImp implements SkuGiftService {

    @Autowired
    private SkuGiftRepository skuGiftRepository;

    private void saveScope(SkusGift skusGift) {
        skuGiftRepository.saveAndFlush(skusGift);
    }

    @Override
    public SkusGift fillAndSaveEntity(SkuGiftResponse skuGiftResponse, Long idPromotion) {
        if(Objects.nonNull(idPromotion)){
            Optional<SkusGift> skusGiftOptional = skuGiftRepository.serchSkusGiftByPromotionAndGift(skuGiftResponse.getQuantitySelectable(), idPromotion);
            if (skusGiftOptional.isPresent()) {
                SkusGift skusGift = SkuGiftAdapter.preencherSkusGift(skusGiftOptional.get(), skuGiftResponse);
                saveScope(skusGift);
                return skusGift;
            }
        }
        SkusGift newSkusGift = SkuGiftAdapter.convertSkuGiftResponseToEntity(skuGiftResponse);
        saveScope(newSkusGift);
        return newSkusGift;
    }
}
