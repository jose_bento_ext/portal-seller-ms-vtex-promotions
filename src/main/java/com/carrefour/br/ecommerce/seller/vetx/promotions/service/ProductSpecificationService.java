package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.util.List;

public interface ProductSpecificationService {
    void fillAndSaveEntity(List<String> productsSpecifications, Promotion promotion);
}
