package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.CampaignPromotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.CampaingPromotionAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.CampaingPromotionRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.CampaingPromotionService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CampaingPromotionServiceImp implements CampaingPromotionService {

    @Autowired
    private CampaingPromotionRepository campaingRepository;

    private void saveStore(CampaignPromotion campaignPromotion) {
        campaingRepository.saveAndFlush(campaignPromotion);
    }

    @Override
    public void fillAndSaveEntity(List<String> campaigns, Promotion promotion) {
        String campaignString = campaigns.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<CampaignPromotion> campaignOptional = campaingRepository.findByPromotionAndCampaign(promotion, campaignString);
            if (campaignOptional.isPresent()) {
                CampaignPromotion store = CampaingPromotionAdapter.preencherCampaignPromotion(campaignOptional.get(), campaignString, promotion);
                saveStore(store);
                return;
            }
        }
        CampaignPromotion newCampaignPromotion = CampaingPromotionAdapter.convertCampaignPromotionToEntity( campaignString, promotion);
        saveStore(newCampaignPromotion);
    }
}
