package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.SkusGift;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.SkuGiftResponse;

public interface SkuGiftService {
    SkusGift fillAndSaveEntity(SkuGiftResponse skuGiftResponse, Long idPromotion);
}
