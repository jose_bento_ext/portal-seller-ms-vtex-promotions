package com.carrefour.br.ecommerce.seller.vetx.promotions.repository;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.RestrictionBin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RestrictionBinRepository extends JpaRepository<RestrictionBin, Long> {
    Optional<RestrictionBin> findByPromotionAndRestrictionBin(Promotion promotion, String store);
}
