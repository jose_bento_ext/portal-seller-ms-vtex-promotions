package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Store;

import java.time.LocalDateTime;
import java.util.Optional;

public class StoreAdapter {

    public static Store convertStoreToEntity(String storeString, Promotion promotion) {
        Store store = getEntity(new Store(), storeString, promotion);
        store.setActive(Boolean.TRUE);
        store.setCreateDate(LocalDateTime.now());
        return store;
    }

    public static Store preencherStore(Store store, String storeString, Promotion promotion) {
        Store newAStore = getEntity(store, storeString, promotion);
        newAStore.setUpdateDate(LocalDateTime.now());
        return newAStore;
    }

    private static Store getEntity(Store store, String storeString, Promotion promotion) {
        store.setStore(Optional.ofNullable(storeString).orElse(null));
        store.setPromotion(promotion);
        return store;
    }
}
