package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.GiftListType;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.GiftListTypeAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.GiftListTypeRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.GiftListTypeService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class GiftListTypeServiceImp implements GiftListTypeService {

    @Autowired
    private GiftListTypeRepository giftListTypeRepository;

    private void saveGiftListType(GiftListType giftListType) {
        giftListTypeRepository.saveAndFlush(giftListType);
    }

    @Override
    public void fillAndSaveEntity(List<String> giftListTypes, Promotion promotion) {
        String giftListTypeString = giftListTypes.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<GiftListType> giftListTypeOptional = giftListTypeRepository.findByPromotionAndType(promotion, giftListTypeString);
            if (giftListTypeOptional.isPresent()) {
                GiftListType giftListType = GiftListTypeAdapter.preencherGiftListType(giftListTypeOptional.get(), giftListTypeString, promotion);
                saveGiftListType(giftListType);
                return;
            }
        }
        GiftListType newGiftListType = GiftListTypeAdapter.convertGiftListTypeToEntity(giftListTypeString, promotion);
        saveGiftListType(newGiftListType);
    }
}
