package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.BrandResponse;

import java.util.List;

public interface BrandService {
    void fillAndSaveEntity(List<BrandResponse> brandResponses, Promotion promotion);
}
