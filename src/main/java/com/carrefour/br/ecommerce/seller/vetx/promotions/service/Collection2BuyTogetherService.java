package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.util.List;

public interface Collection2BuyTogetherService {
    void fillAndSaveEntity(List<String> collection, Promotion promotion);
}
