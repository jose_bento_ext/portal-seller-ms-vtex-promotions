package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.ProductSpecification;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.ProductSpecificationAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.ProductSpecificationRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.ProductSpecificationService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductSpecificationServiceImp implements ProductSpecificationService {

    @Autowired
    private ProductSpecificationRepository productSpecificationRepository;

    private void saveProductSpecification(ProductSpecification productSpecification) {
        productSpecificationRepository.saveAndFlush(productSpecification);
    }

    @Override
    public void fillAndSaveEntity(List<String> productsSpecifications, Promotion promotion) {
        String productSpecificationString = productsSpecifications.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<ProductSpecification> productSpecificationOptional = productSpecificationRepository.findByPromotionAndSpecification(promotion, productSpecificationString);
            if (productSpecificationOptional.isPresent()) {
                ProductSpecification productSpecification = ProductSpecificationAdapter.preencherProductSpecification(productSpecificationOptional.get(), productSpecificationString, promotion);
                saveProductSpecification(productSpecification);
                return;
            }
        }
        ProductSpecification newProductSpecification = ProductSpecificationAdapter.convertProductSpecificationToEntity(productSpecificationString, promotion);
        saveProductSpecification(newProductSpecification);
    }
}
