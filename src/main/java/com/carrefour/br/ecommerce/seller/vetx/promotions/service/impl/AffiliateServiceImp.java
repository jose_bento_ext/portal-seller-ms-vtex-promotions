package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Affiliate;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.AffiliateAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.AffiliateResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.AffiliateRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.AffiliateService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AffiliateServiceImp implements AffiliateService {

    @Autowired
    private AffiliateRepository affiliateRepository;

    private void saveAffiliate(Affiliate brand) {
        affiliateRepository.saveAndFlush(brand);
    }

    @Override
    public void fillAndSaveEntity(List<AffiliateResponse> affiliateResponseList, Promotion promotion) {
        if (Objects.nonNull(promotion)) {
            affiliateResponseList.forEach(affiliateResponse -> {
                Optional<Affiliate> affiliateOptional = affiliateRepository.findByPromotionAndIdVtex(promotion, affiliateResponse.getId());
                Affiliate affiliate = affiliateOptional.isPresent() ? updateAffiliate(affiliateOptional.get(), affiliateResponse, promotion)
                        : newAffiliate(affiliateResponse, promotion);
                saveAffiliate(affiliate);
            });
        }
    }

    private Affiliate newAffiliate(AffiliateResponse affiliateResponse, Promotion promotion) {
        return AffiliateAdapter.convertAffiliateResponseToEntity(affiliateResponse, promotion);
    }

    private Affiliate updateAffiliate(Affiliate affiliate, AffiliateResponse affiliateResponse, Promotion promotion) {
        return AffiliateAdapter.preencherAffiliate(affiliate, affiliateResponse, promotion);
    }
}
