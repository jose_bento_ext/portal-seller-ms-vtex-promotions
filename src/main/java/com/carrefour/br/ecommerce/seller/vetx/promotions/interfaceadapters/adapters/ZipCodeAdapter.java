package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.ZipCode;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.ZipCodeResponse;

import java.time.LocalDateTime;
import java.util.Optional;

public class ZipCodeAdapter {

    public static ZipCode convertZipCodeResponseToEntity(ZipCodeResponse zipCodeResponse, Promotion promotion) {
        ZipCode zipCode = getEntity(new ZipCode(), zipCodeResponse, promotion);
        zipCode.setActive(Boolean.TRUE);
        zipCode.setCreateDate(LocalDateTime.now());
        return zipCode;
    }

    public static ZipCode preencherZipCode(ZipCode zipCode, ZipCodeResponse zipCodeResponse, Promotion promotion) {
        ZipCode newSku = getEntity(zipCode, zipCodeResponse, promotion);
        newSku.setUpdateDate(LocalDateTime.now());
        return newSku;
    }

    private static ZipCode getEntity(ZipCode zipCode, ZipCodeResponse zipCodeResponse, Promotion promotion) {
        zipCode.setZipCodeTo(Optional.ofNullable(zipCodeResponse.getZipCodeTo()).orElse(null));
        zipCode.setZipCodeFrom(Optional.ofNullable(zipCodeResponse.getZipCodeFrom()).orElse(null));
        zipCode.setInclusive(Optional.ofNullable(zipCodeResponse.getInclusive()).orElse(null));
        zipCode.setPromotion(promotion);
        return zipCode;
    }
}
