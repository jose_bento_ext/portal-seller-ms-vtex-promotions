package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.SkusGift;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.SkuGiftResponse;

import java.time.LocalDateTime;
import java.util.Optional;

public class SkuGiftAdapter {

    public static SkusGift convertSkuGiftResponseToEntity(SkuGiftResponse skuGiftResponse) {
        SkusGift skusGift = getEntity(skuGiftResponse, new SkusGift());
        skusGift.setActive(Boolean.TRUE);
        skusGift.setCreateDate(LocalDateTime.now());
        return skusGift;
    }

    public static SkusGift preencherSkusGift(SkusGift skusGift, SkuGiftResponse skuGiftResponse) {
        SkusGift newScope = getEntity(skuGiftResponse, skusGift);
        newScope.setUpdateDate(LocalDateTime.now());
        return newScope;
    }

    private static SkusGift getEntity(SkuGiftResponse skuGiftResponse, SkusGift skusGift) {
        skusGift.setQuantitySelectable(Optional.ofNullable(skuGiftResponse.getQuantitySelectable()).orElse(null));
        skusGift.setGifts(Optional.ofNullable(skuGiftResponse.getGifts()).orElse(null));

        return skusGift;
    }
}
