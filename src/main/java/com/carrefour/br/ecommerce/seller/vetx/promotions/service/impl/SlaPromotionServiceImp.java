package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.SlaPromotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.SlaPromotionAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.SlaPromotionRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.SlaPromotionService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SlaPromotionServiceImp implements SlaPromotionService {

    @Autowired
    private SlaPromotionRepository slaPromotionRepository;

    private void saveStore(SlaPromotion slaPromotion) {
        slaPromotionRepository.saveAndFlush(slaPromotion);
    }

    @Override
    public void fillAndSaveEntity(List<String> slaPromotions, Promotion promotion) {
        String slaPromotionsString = slaPromotions.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<SlaPromotion> slaPromotionOptional = slaPromotionRepository.findByPromotionAndAndSlaId(promotion, slaPromotionsString);
            if (slaPromotionOptional.isPresent()) {
                SlaPromotion slaPromotion = SlaPromotionAdapter.preencherSlaPromotion(slaPromotionOptional.get(), slaPromotionsString, promotion);
                saveStore(slaPromotion);
                return;
            }
        }
        SlaPromotion newSlaPromotion = SlaPromotionAdapter.convertSlaPromotionToEntity(slaPromotionsString, promotion);
        saveStore(newSlaPromotion);
    }
}
