package com.carrefour.br.ecommerce.seller.vetx.promotions.repository;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.ClusterExpression;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClusterExpressionRepository extends JpaRepository<ClusterExpression, Long> {
    Optional<ClusterExpression> findByPromotionAndClusterExpression(Promotion promotion, String clusterExpression);
}
