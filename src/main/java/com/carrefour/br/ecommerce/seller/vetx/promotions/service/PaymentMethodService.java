package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PaymentsMethodsResponse;

import java.util.List;

public interface PaymentMethodService {
    void fillAndSaveEntity(List<PaymentsMethodsResponse> paymentsMethodsResponses, Promotion promotion);
}
