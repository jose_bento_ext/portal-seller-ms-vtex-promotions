package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.SkuResponse;

import java.util.List;

public interface SkuService {
    void fillAndSaveEntity(List<SkuResponse> skuResponses, Promotion promotion);
}
