package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.dto.UserDataDTO;

import java.util.Optional;


public interface UserDataMigrationService {
    Optional<UserDataDTO> findByEmail(String email);
}
