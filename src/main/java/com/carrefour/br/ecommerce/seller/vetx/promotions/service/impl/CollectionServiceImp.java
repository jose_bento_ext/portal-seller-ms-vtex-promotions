package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Collection;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.CollectionAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.CollectionResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.CollectionRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.CollectionService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CollectionServiceImp implements CollectionService {

    @Autowired
    private CollectionRepository collectionRepository;

    private void saveSku(Collection brand) {
        collectionRepository.saveAndFlush(brand);
    }

    @Override
    public void fillAndSaveEntity(List<CollectionResponse> collectionResponses, Promotion promotion) {
        if (Objects.nonNull(promotion)) {
            collectionResponses.forEach(collectionResponse -> {
                Optional<Collection> skuOptional = collectionRepository.findByPromotionAndIdVtex(promotion, collectionResponse.getId());
                Collection collection = skuOptional.isPresent() ? updateSku(skuOptional.get(), collectionResponse, promotion)
                        : newSku(collectionResponse, promotion);
                saveSku(collection);
            });
        }
    }

    private Collection newSku(CollectionResponse collectionResponse, Promotion promotion) {
        return CollectionAdapter.convertSkuResponseToEntity(collectionResponse, promotion);
    }

    private Collection updateSku(Collection sku, CollectionResponse collectionResponse, Promotion promotion) {
        return CollectionAdapter.preencherCollection(sku, collectionResponse, promotion);
    }
}
