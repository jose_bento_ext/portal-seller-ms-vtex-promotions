package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.RestrictionBin;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.RestrictionBinAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.RestrictionBinRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.RestrictionBinService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RestrictionBinServiceImp implements RestrictionBinService {

    @Autowired
    private RestrictionBinRepository restrictionBinRepository;

    private void saveStore(RestrictionBin store) {
        restrictionBinRepository.saveAndFlush(store);
    }

    @Override
    public void fillAndSaveEntity(List<String> restrictionBins, Promotion promotion) {
        String restrictionBinString = restrictionBins.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<RestrictionBin> restrictionBinOptional = restrictionBinRepository.findByPromotionAndRestrictionBin(promotion, restrictionBinString);
            if (restrictionBinOptional.isPresent()) {
                RestrictionBin restrictionBin = RestrictionBinAdapter.preencherRestrictionBin(restrictionBinOptional.get(), restrictionBinString, promotion);
                saveStore(restrictionBin);
                return;
            }
        }
        RestrictionBin newRestrictionBin = RestrictionBinAdapter.convertRestrictionBinToEntity(restrictionBinString, promotion);
        saveStore(newRestrictionBin);
    }
}
