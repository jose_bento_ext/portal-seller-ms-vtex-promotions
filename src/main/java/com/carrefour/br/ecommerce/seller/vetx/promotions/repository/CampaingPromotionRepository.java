package com.carrefour.br.ecommerce.seller.vetx.promotions.repository;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.CampaignPromotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CampaingPromotionRepository extends JpaRepository<CampaignPromotion, Long> {
    Optional<CampaignPromotion> findByPromotionAndCampaign(Promotion promotion, String campaign);
}
