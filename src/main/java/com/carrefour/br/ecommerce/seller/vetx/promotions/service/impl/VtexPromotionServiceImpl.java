package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PromotionResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.VtexPromotionService;
import feign.FeignException;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.cfg.beanvalidation.IntegrationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class VtexPromotionServiceImpl implements VtexPromotionService {

    @Override
    @Transactional
    public PromotionResponse createPromotion(PromotionResponse promotionResponse) throws IntegrationException {
        try {
            /**
             * Adicinar Logica da Criação da Promoção
             */
            return new PromotionResponse();
        } catch (FeignException e) {
            log.error("An error occurred while running query by date", e);
            log.error("Error content Feign: {}", e.contentUTF8());
            throw new IntegrationException("An error occurred while running query by date; status: " + e.status() + " message: " + e.getMessage());
        } catch (Exception e) {
            log.error("There was an unexpected error sending this multiple attribute", e);
            throw new IntegrationException("There was an unexpected error sending this multiple attribute; message: " + e.getMessage());
        }
    }

    @Override
    public PromotionResponse updatePromotion(PromotionResponse promotionResponse, Long idPromotion) {
        try {
            /**
             * Adicinar Logica da Criação da Promoção
             */
            return new PromotionResponse();
        } catch (FeignException e) {
            log.error("An error occurred while running query by date", e);
            log.error("Error content Feign: {}", e.contentUTF8());
            throw new IntegrationException("An error occurred while running query by date; status: " + e.status() + " message: " + e.getMessage());

        } catch (Exception e) {
            log.error("There was an unexpected error sending this multiple attribute", e);
            throw new IntegrationException("There was an unexpected error sending this multiple attribute; message: " + e.getMessage());
        }
    }
}
