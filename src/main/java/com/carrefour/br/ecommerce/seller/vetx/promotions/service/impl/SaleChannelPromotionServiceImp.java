package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.SaleChannelPromotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.SaleChannelPromotionAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.SaleChannelPromotionRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.SaleChannelPromotionService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SaleChannelPromotionServiceImp implements SaleChannelPromotionService {

    @Autowired
    private SaleChannelPromotionRepository saleChannelPromotionRepository;

    private void saveSaleChannelPromotion(SaleChannelPromotion saleChannelPromotion) {
        saleChannelPromotionRepository.saveAndFlush(saleChannelPromotion);
    }

    @Override
    public void fillAndSaveEntity(List<String> saleChannelPromotion, Promotion promotion) {
        String saleChannel = saleChannelPromotion.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<SaleChannelPromotion> saleChannelPromotionOptional = saleChannelPromotionRepository.findByPromotionAndSalesChannel(promotion, saleChannel);
            if (saleChannelPromotionOptional.isPresent()) {
                saveSaleChannelPromotion(SaleChannelPromotionAdapter.preencherSaleChannelPromotion(saleChannelPromotionOptional.get(), saleChannel, promotion));
                return ;
            }
        }
        saveSaleChannelPromotion(SaleChannelPromotionAdapter.convertSaleChannelPromotionToEntity(saleChannel, promotion));
    }
}
