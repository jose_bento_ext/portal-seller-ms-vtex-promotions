package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.exception.exceptionhandler.BusinessRuleException;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PromotionResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.TransactionPromotionService;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.VtexPromotionService;
import com.carrefour.br.ecommerce.seller.vetx.promotions.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.cfg.beanvalidation.IntegrationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TransactionPromontionServiceImpl implements TransactionPromotionService {

    @Autowired
    private VtexPromotionService vtexPromotionService;

    @Override
    public PromotionResponse createPromotion(PromotionResponse promotionResponse) throws IntegrationException, BusinessRuleException {
        /**
         * Validar Contrato com o front-end
         */
        try {
            PromotionResponse promotion = vtexPromotionService.createPromotion(promotionResponse);
            StringBuilder jobDescription = new StringBuilder();
            jobDescription.append("- Vext Promotion: ")
                    .append(" Create Promotion Sucess - ");

            log.info(Constants.JOB_FINISHED + "{}", jobDescription);
            return promotion;
        } catch (Exception e) {
            log.error(e.toString());
            throw new BusinessRuleException(e.getMessage());
        }
    }

    @Override
    public PromotionResponse updatePromotion(PromotionResponse promotionResponse, Long idPromotion) throws BusinessRuleException {
        try {
            /**
             * Validar Contrato com o front-end
             */
            PromotionResponse promotion = vtexPromotionService.updatePromotion(promotionResponse, idPromotion);
            StringBuilder jobDescription = new StringBuilder();
            jobDescription.append("- Vext Promotion: ")
                    .append(" Update Promotion Sucess - ");
            log.info(Constants.JOB_FINISHED + "{}", jobDescription);
            return promotion;
        } catch (Exception e) {
            log.error(e.toString());
            throw new BusinessRuleException(e.getMessage());
        }
    }
}
