package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PromotionResponse;

public interface PromotionService {
    PromotionResponse createdEntity(PromotionResponse promotionResponse);

    PromotionResponse updateEntity(PromotionResponse promotionResponse, Long idPromotion);

}
