package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.MarketingTag;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class MarketingTagAdapter {

    public static MarketingTag convertSaleChannelPromotionToEntity(String marketingtag, Promotion promotion) {
        MarketingTag marketingTag = getEntity(new MarketingTag(), marketingtag, promotion);
        marketingTag.setActive(Boolean.TRUE);
        marketingTag.setCreateDate(LocalDateTime.now());
        return marketingTag;
    }

    public static MarketingTag preencherSaleChannelPromotion(MarketingTag saleChannelPromotion, String marketingtag, Promotion promotion) {
        MarketingTag newSMarketingTag = getEntity(saleChannelPromotion, marketingtag, promotion);
        newSMarketingTag.setUpdateDate(LocalDateTime.now());
        return newSMarketingTag;
    }

    private static MarketingTag getEntity(MarketingTag marketingTag, String marketingtag, Promotion promotion) {
        marketingTag.setMarketingTag(Optional.ofNullable(marketingtag).orElse(null));
        marketingTag.setPromotion(promotion);
        return marketingTag;
    }
}
