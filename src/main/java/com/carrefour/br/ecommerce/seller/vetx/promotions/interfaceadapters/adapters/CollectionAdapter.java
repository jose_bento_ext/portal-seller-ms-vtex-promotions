package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Collection;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.CollectionResponse;

import java.time.LocalDateTime;
import java.util.Optional;

public class CollectionAdapter {

    public static Collection convertSkuResponseToEntity(CollectionResponse collectionResponse, Promotion promotion) {
        Collection collection = getEntity(new Collection(), collectionResponse, promotion);
        collection.setActive(Boolean.TRUE);
        collection.setCreateDate(LocalDateTime.now());
        return collection;
    }

    public static Collection preencherCollection(Collection collection, CollectionResponse collectionResponse, Promotion promotion) {
        Collection newCollection = getEntity(collection, collectionResponse, promotion);
        newCollection.setUpdateDate(LocalDateTime.now());
        return newCollection;
    }

    private static Collection getEntity(Collection collection, CollectionResponse collectionResponse, Promotion promotion) {
        collection.setIdVtex(Optional.ofNullable(collectionResponse.getId()).orElse(null));
        collection.setName(Optional.ofNullable(collectionResponse.getName()).orElse(null));
        collection.setPromotion(promotion);
        return collection;
    }
}
