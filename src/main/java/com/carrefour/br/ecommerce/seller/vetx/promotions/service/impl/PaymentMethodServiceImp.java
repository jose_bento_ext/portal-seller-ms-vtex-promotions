package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.PaymentMethod;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.PaymentMethodAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PaymentsMethodsResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.PaymentMethodRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.PaymentMethodService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PaymentMethodServiceImp implements PaymentMethodService {

    @Autowired
    private PaymentMethodRepository paymentMethodRepository;

    private void savePaymentMethod(PaymentMethod paymentMethod) {
        paymentMethodRepository.saveAndFlush(paymentMethod);
    }

    @Override
    public void fillAndSaveEntity(List<PaymentsMethodsResponse> paymentsMethodsResponses, Promotion promotion) {
        if (Objects.nonNull(promotion)) {
            paymentsMethodsResponses.forEach(paymentsMethodsResponse -> {
                Optional<PaymentMethod> paymentMethodOptional = paymentMethodRepository.findByPromotionAndIdVtex(promotion, paymentsMethodsResponse.getId());
                PaymentMethod paymentMethod = paymentMethodOptional.isPresent() ? updatePaymentMethod(paymentMethodOptional.get(), paymentsMethodsResponse, promotion)
                        : newPaymentMethod(paymentsMethodsResponse, promotion);
                savePaymentMethod(paymentMethod);
            });
        }
    }

    private PaymentMethod newPaymentMethod(PaymentsMethodsResponse paymentsMethodsResponse, Promotion promotion) {
        return PaymentMethodAdapter.convertSaleChannelPromotionToEntity(paymentsMethodsResponse, promotion);
    }

    private PaymentMethod updatePaymentMethod(PaymentMethod paymentMethod, PaymentsMethodsResponse paymentsMethodsResponse, Promotion promotion) {
        return PaymentMethodAdapter.preencherSaleChannelPromotion(paymentMethod, paymentsMethodsResponse, promotion);
    }
}
