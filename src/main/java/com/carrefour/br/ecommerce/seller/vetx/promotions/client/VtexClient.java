package com.carrefour.br.ecommerce.seller.vetx.promotions.client;

import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PromotionResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "${feign.rest.vtex.name}", url = "${feign.rest.vtex.url}")
public interface VtexClient {

    @PostMapping(value = "/api/rnb/pvt/calculatorconfiguration")
    PromotionResponse createOrUpdatePromotion();


}
