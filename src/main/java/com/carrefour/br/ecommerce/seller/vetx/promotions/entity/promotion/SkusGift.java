package com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.EntidadeBase;
import com.carrefour.br.ecommerce.seller.vetx.promotions.utils.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(schema = Constants.SCHEMA_VTEX, name = "sku_gift")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SkusGift extends EntidadeBase<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "quantity_selectable")
    private Integer quantitySelectable;

    @Column(name = "gift", columnDefinition = "jsonb")
    private String gifts;;

    @NotNull
    @Column(name = "active")
    private boolean active;

    @Column(name = "create_date", nullable = false, updatable = false)
    private LocalDateTime createDate;

    @Column(name = "update_date", insertable = false)
    private LocalDateTime updateDate;

}
