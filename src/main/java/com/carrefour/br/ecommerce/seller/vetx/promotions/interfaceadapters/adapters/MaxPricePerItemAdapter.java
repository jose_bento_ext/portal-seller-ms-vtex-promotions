package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.MaxPricePerItem;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class MaxPricePerItemAdapter {

    public static MaxPricePerItem convertMaxPricePerItemToEntity(String maxPrice, Promotion promotion) {
        MaxPricePerItem maxPricePerItem = getEntity(new MaxPricePerItem(), maxPrice, promotion);
        maxPricePerItem.setActive(Boolean.TRUE);
        maxPricePerItem.setCreateDate(LocalDateTime.now());
        return maxPricePerItem;
    }

    public static MaxPricePerItem preencherSkusGift(MaxPricePerItem maxPricePerItem, String maxPrice, Promotion promotion) {
        MaxPricePerItem newMaxPricePerItem = getEntity(maxPricePerItem, maxPrice, promotion);
        newMaxPricePerItem.setUpdateDate(LocalDateTime.now());
        return newMaxPricePerItem;
    }

    private static MaxPricePerItem getEntity(MaxPricePerItem maxPricePerItem, String maxPrice, Promotion promotion) {
        maxPricePerItem.setMaxPricePerItem(Optional.ofNullable(maxPrice).orElse(null));
        maxPricePerItem.setPromotion(promotion);
        return maxPricePerItem;
    }
}
