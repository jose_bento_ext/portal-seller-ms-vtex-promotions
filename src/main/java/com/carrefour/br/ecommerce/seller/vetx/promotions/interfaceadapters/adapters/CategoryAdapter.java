package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Category;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.CategoryResponse;

import java.time.LocalDateTime;
import java.util.Optional;

public class CategoryAdapter {

    public static Category convertCategoryResponseToEntity(CategoryResponse categoryResponse, Promotion promotion) {
        Category paymentMethod = getEntity(new Category(), categoryResponse, promotion);
        paymentMethod.setActive(Boolean.TRUE);
        paymentMethod.setCreateDate(LocalDateTime.now());
        return paymentMethod;
    }

    public static Category preencherCategory(Category category, CategoryResponse categoryResponse, Promotion promotion) {
        Category newCategory = getEntity(category, categoryResponse, promotion);
        newCategory.setUpdateDate(LocalDateTime.now());
        return newCategory;
    }

    private static Category getEntity(Category category, CategoryResponse categoryResponse, Promotion promotion) {
        category.setIdVtex(Optional.ofNullable(categoryResponse.getId()).orElse(null));
        category.setName(Optional.ofNullable(categoryResponse.getName()).orElse(null));
        category.setPromotion(promotion);
        return category;
    }
}
