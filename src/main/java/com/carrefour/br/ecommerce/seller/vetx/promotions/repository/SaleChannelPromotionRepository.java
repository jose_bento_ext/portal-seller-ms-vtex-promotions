package com.carrefour.br.ecommerce.seller.vetx.promotions.repository;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.SaleChannelPromotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SaleChannelPromotionRepository extends JpaRepository<SaleChannelPromotion, Long> {
    Optional<SaleChannelPromotion> findByPromotionAndSalesChannel(Promotion promotion, String idSaleChannel);
}
