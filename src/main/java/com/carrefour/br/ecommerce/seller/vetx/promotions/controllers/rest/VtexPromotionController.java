package com.carrefour.br.ecommerce.seller.vetx.promotions.controllers.rest;

import com.carrefour.br.ecommerce.seller.vetx.promotions.controllers.BaseEndPoint;
import com.carrefour.br.ecommerce.seller.vetx.promotions.exception.exceptionhandler.BusinessRuleException;
import com.carrefour.br.ecommerce.seller.vetx.promotions.i18n.MessageI18N;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PromotionResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.TransactionPromotionService;
import feign.FeignException;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.UUID;

@RestController
@RequestMapping(value = "/portal-seller/vetex/promotion")
@AllArgsConstructor
@Slf4j
public class VtexPromotionController extends BaseEndPoint {

    @Autowired
    private TransactionPromotionService transactionMigrationService;

    @ApiOperation(value = "Criação de uma nova Promoção", notes = "Cria uma nova Promoção")
    @PostMapping("")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPromotion(@Valid @RequestBody PromotionResponse promotionResponse) {
        Response response;
        try {
            response =  Response.ok(transactionMigrationService.createPromotion(promotionResponse)).build();

        } catch (FeignException feignException) {
            log.error("Error getting attributes on Mirikl {} status: {} message: {}",
                    feignException.status(),
                    feignException.getMessage());
            log.error("Error content Feign: {}", feignException.contentUTF8());
            BusinessRuleException regraNegocioException = new BusinessRuleException();
            regraNegocioException.addMesage(MessageI18N.getKey("general.fail.generate.transaction.snapshot"));
            response = fromResponseDeBusinessRuleException(regraNegocioException);

        } catch (BusinessRuleException e) {
            log.error("Error getting token, Message: {}", e.getMessage(), e);
            BusinessRuleException regraNegocioException = new BusinessRuleException();
            regraNegocioException.addMesage(MessageI18N.getKey("general.fail.migration"));
            response = fromResponseDeBusinessRuleException(regraNegocioException);

        } catch (Exception e) {
            log.error("Error when trying to rec product attribute, Message: {}", e.getMessage(), e);
            BusinessRuleException regraNegocioException = new BusinessRuleException();
            regraNegocioException.addMesage(MessageI18N.getKey("general.fail.migration"));
            response = fromResponseDeBusinessRuleException(regraNegocioException);
        }

        return response;
    }

    @ApiOperation(value = "Alteração de uma Promoção", notes = "Edita uma nova Promoção")
    @PatchMapping("/{idPromotion}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updatePromotion(@Valid @RequestBody PromotionResponse promotionResponse, @PathVariable Long idPromotion) {
        Response response;
        try {
            response = Response.ok(this.transactionMigrationService.updatePromotion(promotionResponse, idPromotion)).build();

        } catch (FeignException feignException) {
            log.error("Error getting attributes on Mirikl {} status: {} message: {}",
                    feignException.status(),
                    feignException.getMessage());
            log.error("Error content Feign: {}", feignException.contentUTF8());
            BusinessRuleException regraNegocioException = new BusinessRuleException();
            regraNegocioException.addMesage(MessageI18N.getKey("general.fail.generate.transaction.snapshot"));
            response = fromResponseDeBusinessRuleException(regraNegocioException);

        } catch (BusinessRuleException e) {
            log.error("Error getting token, Message: {}", e.getMessage(), e);
            BusinessRuleException regraNegocioException = new BusinessRuleException();
            regraNegocioException.addMesage(MessageI18N.getKey("general.fail.migration"));
            response = fromResponseDeBusinessRuleException(regraNegocioException);

        } catch (Exception e) {
            log.error("Error when trying to rec product attribute, Message: {}", e.getMessage(), e);
            BusinessRuleException regraNegocioException = new BusinessRuleException();
            regraNegocioException.addMesage(MessageI18N.getKey("general.fail.migration"));
            response = fromResponseDeBusinessRuleException(regraNegocioException);
        }

        return response;
    }
}
