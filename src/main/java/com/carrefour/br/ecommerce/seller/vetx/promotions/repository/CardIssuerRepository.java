package com.carrefour.br.ecommerce.seller.vetx.promotions.repository;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.CardIssuer;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CardIssuerRepository extends JpaRepository<CardIssuer, Long> {
    Optional<CardIssuer> findByPromotionAndCardIssuer(Promotion promotion, String store);
}
