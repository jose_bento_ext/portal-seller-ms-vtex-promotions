package com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.EntidadeBase;
import com.carrefour.br.ecommerce.seller.vetx.promotions.utils.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(schema = Constants.SCHEMA_VTEX, name = "max_price_per_item")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MaxPricePerItem extends EntidadeBase<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_promotion")
    private Promotion promotion;

    @Column(name = "max_price_per_item")
    private String maxPricePerItem;

    @NotNull
    @Column(name = "active")
    private boolean active;

    @Column(name = "create_date", nullable = false, updatable = false)
    private LocalDateTime createDate;

    @Column(name = "update_date", insertable = false)
    private LocalDateTime updateDate;

}
