package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Merchant;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class MerchantAdapter {

    public static Merchant convertMerchantToEntity(String merchantString, Promotion promotion) {
        Merchant merchant = getEntity(new Merchant(), merchantString, promotion);
        merchant.setActive(Boolean.TRUE);
        merchant.setCreateDate(LocalDateTime.now());
        return merchant;
    }

    public static Merchant preencherMerchant(Merchant merchant, String merchantString, Promotion promotion) {
        Merchant newMerchant = getEntity(merchant, merchantString, promotion);
        newMerchant.setUpdateDate(LocalDateTime.now());
        return newMerchant;
    }

    private static Merchant getEntity(Merchant merchant, String merchantString, Promotion promotion) {
        merchant.setMerchant(Optional.ofNullable(merchantString).orElse(null));
        merchant.setPromotion(promotion);
        return merchant;
    }
}
