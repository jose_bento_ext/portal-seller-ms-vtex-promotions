package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PromotionResponse;

public interface VtexPromotionService {
    PromotionResponse createPromotion(PromotionResponse promotionResponse);
    PromotionResponse updatePromotion(PromotionResponse promotionResponse, Long idPromotion);
}
