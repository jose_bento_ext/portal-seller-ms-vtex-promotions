package com.carrefour.br.ecommerce.seller.vetx.promotions.repository;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.config.UserDataMigration;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.dto.UserDataDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDataMigrationRepository extends JpaRepository<UserDataMigration, Long>, JpaSpecificationExecutor<UserDataMigration> {

    UserDataDTO findByEmail(String email);
}

