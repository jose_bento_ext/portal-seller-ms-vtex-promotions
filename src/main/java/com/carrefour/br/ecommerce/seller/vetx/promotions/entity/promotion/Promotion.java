package com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion;


import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.EntidadeBase;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.enums.EffectTypeEnum;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.enums.TotalValueModeEnum;
import com.carrefour.br.ecommerce.seller.vetx.promotions.utils.Constants;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(schema = Constants.SCHEMA_VTEX, name = "promotion")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Promotion extends EntidadeBase<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_calculator_configuration")
    private String idCalculatorConfiguration;

    @Column(name = "name_promotion")
    private String namePromotion;

    @Column(name = "name_promotion_seller")
    private String namePromotionSeller;

    @Column(name = "description")
    private String description;

    @Column(name = "begin_date_utc")
    private LocalDateTime beginDateUtc;

    @Column(name = "end_date_utc")
    private LocalDateTime endDateUtc;

    @Column(name = "last_modified")
    private LocalDateTime lastModified;

    @Column(name = "days_ago_of_purchases")
    private Integer daysAgoOfPurchases;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "is_archived")
    private Boolean isArchived;

    @Column(name = "is_featured")
    private Boolean isFeatured;

    @Column(name = "disable_deal")
    private Boolean disableDeal;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<ActiveDaysOfWeek> activeDaysOfWeek;

    @Column(name = "offset_promotion")
    private Integer offset;

    @Column(name = "activate_gifts_multiplier")
    private Boolean activateGiftsMultiplier;

    @Column(name = "new_offset")
    private BigDecimal newOffset;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<MaxPricePerItem> maxPricesPerItems;

    @Column(name = "cumulative")
    private Boolean cumulative;

    @Column(name = "effect_type")
    private EffectTypeEnum effectType;

    @Column(name = "discount_type")
    private String discountType;

    @Column(name = "nominal_shipping_discount_value")
    private BigDecimal nominalShippingDiscountValue;

    @Column(name = "absolute_shipping_discount_value")
    private BigDecimal absoluteShippingDiscountValue;

    @Column(name = "nominal_discount_value")
    private BigDecimal nominalDiscountValue;

    @Column(name = "maximum_unitprice_discount")
    private BigDecimal maximumUnitPriceDiscount;

    @Column(name = "percentual_discount_value")
    private BigDecimal percentualDiscountValue;

    @Column(name = "rebate_percentual_discount_value")
    private BigDecimal rebatePercentualDiscountValue;

    @Column(name = "percentual_shipping_discount_value")
    private BigDecimal percentualShippingDiscountValue;

    @Column(name = "percentual_tax")
    private BigDecimal percentualTax;

    @Column(name = "shipping_percentual_tax")
    private BigDecimal shippingPercentualTax;

    @Column(name = "percentual_discount_value_list1")
    private BigDecimal percentualDiscountValueList1;

    @Column(name = "percentual_discount_value_list2")
    private BigDecimal percentualDiscountValueList2;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sku_gift")
    private SkusGift skusGift;

    @Column(name = "nominal_reward_value")
    private BigDecimal nominalRewardValue;

    @Column(name = "percentual_reward_value")
    private BigDecimal percentualRewardValue;

    @Column(name = "order_status_reward_value")
    private String orderStatusRewardValue;

    @Column(name = "max_number_of_affected_items")
    private Integer maxNumberOfAffectedItems;

    @Column(name = "max_number_of_affected_items_group_key")
    private String maxNumberOfAffectedItemsGroupKey;

    @Column(name = "apply_to_all_shippings")
    private Boolean applyToAllShippings;

    @Column(name = "nominal_tax")
    private BigDecimal nominalTax;

    @Column(name = "origin")
    private String origin;

    @Column(name = "id_seller")
    private String idSeller;

    @Column(name = "id_seller_is_inclusive")
    private Boolean idSellerIsInclusive;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<SaleChannelPromotion> idsSaleChannelPromotion;

    @Column(name = "are_sales_channel_ids_exclusive")
    private Boolean areSalesChannelIdsExclusive;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<MarketingTag> marketingTags;

    @Column(name = "marketing_tags_are_not_inclusive")
    private Boolean marketingTagsAreNotInclusive;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<PaymentMethod> paymentsMethods;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<Store> stores;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<CampaignPromotion> campaigns;

    @Column(name = "stores_are_inclusive")
    private Boolean storesAreInclusive;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<Category> categories;

    @Column(name = "categories_are_inclusive")
    private Boolean categoriesAreInclusive;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<Brand> brands;

    @Column(name = "brands_are_inclusive")
    private Boolean brandsAreInclusive;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<Product> products;

    @Column(name = "products_are_inclusive")
    private Boolean productsAreInclusive;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<Sku> skus;

    @Column(name = "skus_are_inclusive")
    private Boolean skusAreInclusive;

    @Column(name = "utm_source")
    private String utmSource;

    @Column(name = "utm_campaign")
    private String utmCampaign;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<Collection2BuyTogether> collections2BuyTogether;

    @Column(name = "minimum_quantity_buy_together")
    private Integer minimumQuantityBuyTogether;

    @Column(name = "quantity_to_affect_buy_together")
    private Integer quantityToAffectBuyTogether;

    @Column(name = "enable_buy_together_per_sku")
    private Boolean enableBuyTogetherPerSku;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<Coupon> coupon;

    @Column(name = "total_value_floor")
    private BigDecimal totalValueFloor;

    @Column(name = "total_value_celing")
    private BigDecimal totalValueCeling;

    @Column(name = "total_value_include_all_items")
    private Boolean totalValueIncludeAllItems;

    @Column(name = "total_value_mode")
    private TotalValueModeEnum totalValueMode;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<Collection> collections;

    @Column(name = "collections_is_inclusive")
    private Boolean collectionsIsInclusive;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<RestrictionBin> restrictionsBins;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<CardIssuer> cardIssuers;

    @Column(name = "total_value_purchase")
    private BigDecimal totalValuePurchase;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<SlaPromotion> slasIds;

    @Column(name = "is_sla_selected")
    private Boolean isSlaSelected;

    @Column(name = "is_first_buy")
    private Boolean isFirstBuy;

    @Column(name = "first_buy_is_profile_optimistic")
    private Boolean firstBuyIsProfileOptimistic;

    @Column(name = "compare_list_price_and_price")
    private Boolean compareListPriceAndPrice;

    @Column(name = "is_different_list_price_and_price")
    private Boolean isDifferentListPriceAndPrice;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<ZipCode> zipCodeRanges;

    @Column(name = "item_max_price")
    private BigDecimal itemMaxPrice;

    @Column(name = "item_min_price")
    private BigDecimal itemMinPrice;

    @Column(name = "installment")
    private Integer installment;

    @Column(name = "is_min_max_installments")
    private Boolean isMinMaxInstallments;

    @Column(name = "min_installment")
    private Integer minInstallment;

    @Column(name = "max_installment")
    private Integer maxInstallment;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<Merchant> merchants;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<ClusterExpression> clusterExpressions;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<PaymentRule> paymentsRules;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<GiftListType> giftListTypes;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<ProductSpecification> productsSpecifications;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "promotion")
    private List<Affiliate> affiliates;

    @Column(name = "max_usage")
    private Integer maxUsage;

    @Column(name = "max_usage_per_client")
    private Integer maxUsagePerClient;

    @Column(name = "should_distribute_discount_among_matched_items")
    private Boolean shouldDistributeDiscountAmongMatchedItems;

    @Column(name = "multiple_use_per_client")
    private Boolean multipleUsePerClient;

    @Column(name = "accumulate_with_manual_price")
    private Boolean accumulateWithManualPrice;

    @Column(name = "type_promotion")
    private String type;

    @Column(name = "use_new_progressive_algorithm")
    private Boolean useNewProgressiveAlgorithm;

    @NotNull
    @Column(name = "active")
    private boolean active;

    @Column(name = "create_date", nullable = false, updatable = false)
    private LocalDateTime createDate;

    @Column(name = "update_date", insertable = false)
    private LocalDateTime updateDate;

}
