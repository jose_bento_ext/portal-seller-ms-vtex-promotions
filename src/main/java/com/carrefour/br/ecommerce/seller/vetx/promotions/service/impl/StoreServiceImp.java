package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Store;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.StoreAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.StoreRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.StoreService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class StoreServiceImp implements StoreService {

    @Autowired
    private StoreRepository storeRepository;

    private void saveStore(Store store) {
        storeRepository.saveAndFlush(store);
    }

    @Override
    public void fillAndSaveEntity(List<String> stores, Promotion promotion) {
        String storeString = stores.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<Store> storeOptional = storeRepository.findByPromotionAndStore(promotion, storeString);
            if (storeOptional.isPresent()) {
                Store store = StoreAdapter.preencherStore(storeOptional.get(), storeString, promotion);
                saveStore(store);
                return;
            }
        }
        Store newStore = StoreAdapter.convertStoreToEntity(storeString, promotion);
        saveStore(newStore);
    }
}
