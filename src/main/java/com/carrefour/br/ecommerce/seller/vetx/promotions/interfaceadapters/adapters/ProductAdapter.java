package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Product;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.ProductResponse;

import java.time.LocalDateTime;
import java.util.Optional;

public class ProductAdapter {

    public static Product convertProductResponseToEntity(ProductResponse productResponse, Promotion promotion) {
        Product paymentMethod = getEntity(new Product(), productResponse, promotion);
        paymentMethod.setActive(Boolean.TRUE);
        paymentMethod.setCreateDate(LocalDateTime.now());
        return paymentMethod;
    }

    public static Product preencherProduct(Product product, ProductResponse productResponse, Promotion promotion) {
        Product newProduct = getEntity(product, productResponse, promotion);
        newProduct.setUpdateDate(LocalDateTime.now());
        return newProduct;
    }

    private static Product getEntity(Product product, ProductResponse productResponse, Promotion promotion) {
        product.setIdVtex(Optional.ofNullable(productResponse.getId()).orElse(null));
        product.setName(Optional.ofNullable(productResponse.getName()).orElse(null));
        product.setPromotion(promotion);
        return product;
    }
}
