package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.PaymentMethod;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.PaymentsMethodsResponse;

import java.time.LocalDateTime;
import java.util.Optional;

public class PaymentMethodAdapter {

    public static PaymentMethod convertSaleChannelPromotionToEntity(PaymentsMethodsResponse paymentsMethodsResponse, Promotion promotion) {
        PaymentMethod paymentMethod = getEntity(new PaymentMethod(), paymentsMethodsResponse, promotion);
        paymentMethod.setActive(Boolean.TRUE);
        paymentMethod.setCreateDate(LocalDateTime.now());
        return paymentMethod;
    }

    public static PaymentMethod preencherSaleChannelPromotion(PaymentMethod paymentMethod, PaymentsMethodsResponse paymentsMethodsResponse, Promotion promotion) {
        PaymentMethod newPaymentMethod = getEntity(paymentMethod, paymentsMethodsResponse, promotion);
        newPaymentMethod.setUpdateDate(LocalDateTime.now());
        return newPaymentMethod;
    }

    private static PaymentMethod getEntity(PaymentMethod paymentMethod, PaymentsMethodsResponse paymentsMethodsResponse, Promotion promotion) {
        paymentMethod.setIdVtex(Optional.ofNullable(paymentsMethodsResponse.getId()).orElse(null));
        paymentMethod.setName(Optional.ofNullable(paymentsMethodsResponse.getName()).orElse(null));
        paymentMethod.setPromotion(promotion);
        return paymentMethod;
    }
}
