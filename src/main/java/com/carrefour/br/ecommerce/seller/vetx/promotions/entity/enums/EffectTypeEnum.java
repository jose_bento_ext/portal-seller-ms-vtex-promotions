package com.carrefour.br.ecommerce.seller.vetx.promotions.entity.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@JsonFormat(shape = Shape.OBJECT)
public enum EffectTypeEnum implements EnumConverter<String> {
    PRICE("price", "Price"),
    GIVEAWAY("giveaway", "Giveaway"),
    SHIPPING("shipping", "Shipping"),
    NO_DATA("no_data", "No Data");


    private String codigo;

    private String descricao;

    EffectTypeEnum(final String codigo, final String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    @Override
    public String getCodigo() {
        return this.codigo;
    }

    public final String getDescricao() {
        return this.descricao;
    }
}
