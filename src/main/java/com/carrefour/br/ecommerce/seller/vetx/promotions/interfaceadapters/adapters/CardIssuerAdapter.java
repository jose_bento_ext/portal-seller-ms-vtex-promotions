package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.CardIssuer;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class CardIssuerAdapter {

    public static CardIssuer convertRestrictionBinToEntity(String cardIssuerString, Promotion promotion) {
        CardIssuer cardIssuer = getEntity(new CardIssuer(), cardIssuerString, promotion);
        cardIssuer.setActive(Boolean.TRUE);
        cardIssuer.setCreateDate(LocalDateTime.now());
        return cardIssuer;
    }

    public static CardIssuer preencherRestrictionBin(CardIssuer cardIssuer, String cardIssuerString, Promotion promotion) {
        CardIssuer newCardIssuer = getEntity(cardIssuer, cardIssuerString, promotion);
        newCardIssuer.setUpdateDate(LocalDateTime.now());
        return newCardIssuer;
    }

    private static CardIssuer getEntity(CardIssuer cardIssuer, String cardIssuerString, Promotion promotion) {
        cardIssuer.setCardIssuer(Optional.ofNullable(cardIssuerString).orElse(null));
        cardIssuer.setPromotion(promotion);
        return cardIssuer;
    }
}
