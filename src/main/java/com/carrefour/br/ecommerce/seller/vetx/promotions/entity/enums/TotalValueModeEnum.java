package com.carrefour.br.ecommerce.seller.vetx.promotions.entity.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@JsonFormat(shape = Shape.OBJECT)
public enum TotalValueModeEnum implements EnumConverter<String> {
    INCLUDE_MATCHED_ITEMS("IncludeMatchedItems", "Include Matched Items"),
    EXCLUDE_MATCHED_ITEMS("ExcludeMatchedItems", "Exclude Matched Items"),
    ALLI_TEMS("AllItems", "All Items"),
    NO_TEMS("NoItems", "No Items");


    private String codigo;

    private String descricao;

    TotalValueModeEnum(final String codigo, final String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    @Override
    public String getCodigo() {
        return this.codigo;
    }

    public final String getDescricao() {
        return this.descricao;
    }
}
