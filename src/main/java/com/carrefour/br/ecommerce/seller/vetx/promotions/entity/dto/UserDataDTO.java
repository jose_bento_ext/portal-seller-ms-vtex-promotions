package com.carrefour.br.ecommerce.seller.vetx.promotions.entity.dto;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.config.UserDataMigration;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.enums.ProfileEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDataDTO {
    private Long code;
    private String email;
    private String password;
    private ProfileEnum profile;

    public UserDataDTO(UserDataMigration entity) {
        this.code = entity.getId();
        this.email = entity.getEmail();
        this.password = entity.getPassword();
        this.profile = entity.getProfile();
    }
}
