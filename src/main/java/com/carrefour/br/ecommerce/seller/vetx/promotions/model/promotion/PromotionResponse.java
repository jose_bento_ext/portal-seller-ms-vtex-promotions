package com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PromotionResponse {

    @JsonProperty("id")
    private String idCalculatorConfiguration;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("beginDateUtc")
    private LocalDateTime beginDateUtc;

    @JsonProperty("endDateUtc")
    private LocalDateTime endDateUtc;

    @JsonProperty("lastModified")
    private LocalDateTime lastModified;

    @JsonProperty("daysAgoOfPurchases")
    private Integer daysAgoOfPurchases;

    @JsonProperty("isActive")
    private Boolean isActive;

    @JsonProperty("isArchived")
    private Boolean isArchived;

    @JsonProperty("isFeatured")
    private Boolean isFeatured;

    @JsonProperty("disableDeal")
    private Boolean disableDeal;

    @JsonProperty("activeDaysOfWeek")
    private List<String> activeDaysOfWeek;

    @JsonProperty("offset")
    private Integer offset;

    @JsonProperty("activateGiftsMultiplier")
    private Boolean activateGiftsMultiplier;

    @JsonProperty("newOffset")
    private BigDecimal newOffset;

    @JsonProperty("maxPricesPerItems")
    private List<String> maxPricesPerItems;

    @JsonProperty("cumulative")
    private Boolean cumulative;

    @JsonProperty("effectType")
    private String effectType;

    @JsonProperty("discountType")
    private String discountType;

    @JsonProperty("nominalShippingDiscountValue")
    private BigDecimal nominalShippingDiscountValue;

    @JsonProperty("absoluteShippingDiscountValue")
    private BigDecimal absoluteShippingDiscountValue;

    @JsonProperty("nominalDiscountValue")
    private BigDecimal nominalDiscountValue;

    @JsonProperty("maximumUnitPriceDiscount")
    private BigDecimal maximumUnitPriceDiscount;

    @JsonProperty("percentualDiscountValue")
    private BigDecimal percentualDiscountValue;

    @JsonProperty("rebatePercentualDiscountValue")
    private BigDecimal rebatePercentualDiscountValue;

    @JsonProperty("percentualShippingDiscountValue")
    private BigDecimal percentualShippingDiscountValue;

    @JsonProperty("percentualTax")
    private BigDecimal percentualTax;

    @JsonProperty("shippingPercentualTax")
    private BigDecimal shippingPercentualTax;

    @JsonProperty("percentualDiscountValueList1")
    private BigDecimal percentualDiscountValueList1;

    @JsonProperty("percentualDiscountValueList2")
    private BigDecimal percentualDiscountValueList2;

    @JsonProperty("skusGift")
    private SkuGiftResponse skusGift;

    @JsonProperty("nominalRewardValue")
    private BigDecimal nominalRewardValue;

    @JsonProperty("percentualRewardValue")
    private BigDecimal percentualRewardValue;

    @JsonProperty("orderStatusRewardValue")
    private String orderStatusRewardValue;

    @JsonProperty("maxNumberOfAffectedItems")
    private Integer maxNumberOfAffectedItems;

    @JsonProperty("maxNumberOfAffectedItemsGroupKey")
    private String maxNumberOfAffectedItemsGroupKey;

    @JsonProperty("applyToAllShippings")
    private Boolean applyToAllShippings;

    @JsonProperty("nominalTax")
    private BigDecimal nominalTax;

    @JsonProperty("origin")
    private String origin;

    @JsonProperty("idSeller")
    private String idSeller;

    @JsonProperty("idSellerIsInclusive")
    private Boolean idSellerIsInclusive;

    @JsonProperty("idsSalesChannel")
    private List<String> idsSalesChannel;

    @JsonProperty("areSalesChannelIdsExclusive")
    private Boolean areSalesChannelIdsExclusive;

    @JsonProperty("marketingTags")
    private List<String> marketingTags;

    @JsonProperty("marketingTagsAreNotInclusive")
    private Boolean marketingTagsAreNotInclusive;

    @JsonProperty("paymentsMethods")
    private List<PaymentsMethodsResponse> paymentsMethods;

    @JsonProperty("stores")
    private List<String> stores;

    @JsonProperty("campaigns")
    private List<String> campaigns;

    @JsonProperty("storesAreInclusive")
    private Boolean storesAreInclusive;

    @JsonProperty("categories")
    private List<CategoryResponse> categories;

    @JsonProperty("categoriesAreInclusive")
    private Boolean categoriesAreInclusive;

    @JsonProperty("brands")
    private List<BrandResponse> brands;

    @JsonProperty("brandsAreInclusive")
    private Boolean brandsAreInclusive;

    @JsonProperty("products")
    private List<ProductResponse> products;

    @JsonProperty("productsAreInclusive")
    private Boolean productsAreInclusive;

    @JsonProperty("skus")
    private List<SkuResponse> skus;

    @JsonProperty("skusAreInclusive")
    private Boolean skusAreInclusive;

    @JsonProperty("utmSource")
    private String utmSource;

    @JsonProperty("utmCampaign")
    private String utmCampaign;

    @JsonProperty("collections2BuyTogether")
    private List<String> collections2BuyTogether;

    @JsonProperty("minimumQuantityBuyTogether")
    private Integer minimumQuantityBuyTogether;

    @JsonProperty("quantityToAffectBuyTogether")
    private Integer quantityToAffectBuyTogether;

    @JsonProperty("enableBuyTogetherPerSku")
    private Boolean enableBuyTogetherPerSku;

    @JsonProperty("coupon")
    private List<String> coupon;

    @JsonProperty("totalValueFloor")
    private BigDecimal totalValueFloor;

    @JsonProperty("totalValueCeling")
    private BigDecimal totalValueCeling;

    @JsonProperty("totalValueIncludeAllItems")
    private Boolean totalValueIncludeAllItems;

    @JsonProperty("totalValueMode")
    private String totalValueMode;

    @JsonProperty("collections")
    private List<CollectionResponse> collections;

    @JsonProperty("collectionsIsInclusive")
    private Boolean collectionsIsInclusive;

    @JsonProperty("restrictionsBins")
    private List<String> restrictionsBins;

    @JsonProperty("cardIssuers")
    private List<String> cardIssuers;

    @JsonProperty("totalValuePurchase")
    private BigDecimal totalValuePurchase;

    @JsonProperty("slasIds")
    private List<String> slasIds;

    @JsonProperty("isSlaSelected")
    private Boolean isSlaSelected;

    @JsonProperty("isFirstBuy")
    private Boolean isFirstBuy;

    @JsonProperty("firstBuyIsProfileOptimistic")
    private Boolean firstBuyIsProfileOptimistic;

    @JsonProperty("compareListPriceAndPrice")
    private Boolean compareListPriceAndPrice;

    @JsonProperty("isDifferentListPriceAndPrice")
    private Boolean isDifferentListPriceAndPrice;

    @JsonProperty("zipCodeRanges")
    private List<ZipCodeResponse> zipCodeRanges;

    @JsonProperty("itemMaxPrice")
    private BigDecimal itemMaxPrice;

    @JsonProperty("itemMinPrice")
    private BigDecimal itemMinPrice;

    @JsonProperty("installment")
    private Integer installment;

    @JsonProperty("isMinMaxInstallments")
    private Boolean isMinMaxInstallments;

    @JsonProperty("minInstallment")
    private Integer minInstallment;

    @JsonProperty("maxInstallment")
    private Integer maxInstallment;

    @JsonProperty("merchants")
    private List<String> merchants;

    @JsonProperty("clusterExpressions")
    private List<String> clusterExpressions;

    @JsonProperty("clusterOperator")
    private String clusterOperator;

    @JsonProperty("paymentsRules")
    private List<String> paymentsRules;

    @JsonProperty("giftListTypes")
    private List<String> giftListTypes;

    @JsonProperty("productsSpecifications")
    private List<String> productsSpecifications;

    @JsonProperty("affiliates")
    private List<AffiliateResponse> affiliates;

    @JsonProperty("maxUsage")
    private Integer maxUsage;

    @JsonProperty("maxUsagePerClient")
    private Integer maxUsagePerClient;

    @JsonProperty("shouldDistributeDiscountAmongMatchedItems")
    private Boolean shouldDistributeDiscountAmongMatchedItems;

    @JsonProperty("multipleUsePerClient")
    private Boolean multipleUsePerClient;

    @JsonProperty("accumulateWithManualPrice")
    private Boolean accumulateWithManualPrice;

    @JsonProperty("type")
    private String type;

    @JsonProperty("useNewProgressiveAlgorithm")
    private Boolean useNewProgressiveAlgorithm;

}
