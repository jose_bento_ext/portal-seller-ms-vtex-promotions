package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.util.List;

public interface PaymentRuleService {
    void fillAndSaveEntity(List<String> paymentRules, Promotion promotion);
}
