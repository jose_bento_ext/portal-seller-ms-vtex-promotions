package com.carrefour.br.ecommerce.seller.vetx.promotions.entity.config;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.EntidadeBase;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.enums.ProfileEnum;
import com.carrefour.br.ecommerce.seller.vetx.promotions.utils.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(schema = Constants.SCHEMA_PUBLIC, name = "user_data_migration")
public class UserDataMigration extends EntidadeBase<Long> {

    private static final long serialVersionUID = -9052191466844207045L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "profile")
    private ProfileEnum profile;

    @NotNull
    @Column(name = "active")
    private boolean active;

    @Column(name = "create_date", nullable = false, updatable = false)
    private LocalDateTime createDate;

    @Column(name = "update_date", insertable = false)
    private LocalDateTime updateDate;
}
