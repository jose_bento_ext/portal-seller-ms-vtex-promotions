package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Brand;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.BrandResponse;

import java.time.LocalDateTime;
import java.util.Optional;

public class BrandAdapter {

    public static Brand convertBrandResponseToEntity(BrandResponse brandResponse, Promotion promotion) {
        Brand paymentMethod = getEntity(new Brand(), brandResponse, promotion);
        paymentMethod.setActive(Boolean.TRUE);
        paymentMethod.setCreateDate(LocalDateTime.now());
        return paymentMethod;
    }

    public static Brand preencherBrand(Brand brand, BrandResponse brandResponse, Promotion promotion) {
        Brand newBrand = getEntity(brand, brandResponse, promotion);
        newBrand.setUpdateDate(LocalDateTime.now());
        return newBrand;
    }

    private static Brand getEntity(Brand brand, BrandResponse brandResponse, Promotion promotion) {
        brand.setIdVtex(Optional.ofNullable(brandResponse.getId()).orElse(null));
        brand.setName(Optional.ofNullable(brandResponse.getName()).orElse(null));
        brand.setPromotion(promotion);
        return brand;
    }
}
