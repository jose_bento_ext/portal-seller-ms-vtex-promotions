package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.MaxPricePerItem;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.MaxPricePerItemAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.MaxPricePerItemRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.MaxPricePerItemService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MaxPricePerItemServiceImp implements MaxPricePerItemService {

    @Autowired
    private MaxPricePerItemRepository maxPricePerItemRepository;

    private void saveActiveDaysOfWeek(MaxPricePerItem maxPricePerItem) {
        maxPricePerItemRepository.saveAndFlush(maxPricePerItem);
    }

    @Override
    public void fillAndSaveEntity(List<String> maxPricePerItemList, Promotion promotion) {
        String maxPrice = maxPricePerItemList.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<MaxPricePerItem> maxPricePerItemOptional = maxPricePerItemRepository.findByPromotionAndMaxPricePerItem(promotion, maxPrice);
            if (maxPricePerItemOptional.isPresent()) {
                MaxPricePerItem maxPricePerItem = MaxPricePerItemAdapter.preencherSkusGift(maxPricePerItemOptional.get(), maxPrice, promotion);
                saveActiveDaysOfWeek(maxPricePerItem);
                return;
            }
        }
        MaxPricePerItem newMaxPricePerItem = MaxPricePerItemAdapter.convertMaxPricePerItemToEntity(maxPrice, promotion);
        saveActiveDaysOfWeek(newMaxPricePerItem);
    }
}
