package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.CampaignPromotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class CampaingPromotionAdapter {

    public static CampaignPromotion convertCampaignPromotionToEntity(String campaignPromotionString, Promotion promotion) {
        CampaignPromotion campaignPromotion = getEntity(new CampaignPromotion(), campaignPromotionString, promotion);
        campaignPromotion.setActive(Boolean.TRUE);
        campaignPromotion.setCreateDate(LocalDateTime.now());
        return campaignPromotion;
    }

    public static CampaignPromotion preencherCampaignPromotion(CampaignPromotion campaignPromotion, String campaignPromotionString, Promotion promotion) {
        CampaignPromotion newCampaignPromotion = getEntity(campaignPromotion, campaignPromotionString, promotion);
        newCampaignPromotion.setUpdateDate(LocalDateTime.now());
        return newCampaignPromotion;
    }

    private static CampaignPromotion getEntity(CampaignPromotion campaignPromotion, String campaignPromotionString, Promotion promotion) {
        campaignPromotion.setCampaign(Optional.ofNullable(campaignPromotionString).orElse(null));
        campaignPromotion.setPromotion(promotion);
        return campaignPromotion;
    }
}
