package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Category;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.CategoryAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.CategoryResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.CategoryRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CategoryServiceImp implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    private void saveStore(Category category) {
        categoryRepository.saveAndFlush(category);
    }

    @Override
    public void fillAndSaveEntity(List<CategoryResponse> categoryResponses, Promotion promotion) {
        if (Objects.nonNull(promotion)) {
            categoryResponses.forEach(categoryResponse -> {
                Optional<Category> categoryOptional = categoryRepository.findByPromotionAndIdVtex(promotion, categoryResponse.getId());
                Category category = categoryOptional.isPresent() ? updateCategory(categoryOptional.get(), categoryResponse, promotion)
                        : newCategory(categoryResponse, promotion);
                saveStore(category);
            });
        }
    }

    private Category newCategory(CategoryResponse categoryResponse, Promotion promotion) {
        return CategoryAdapter.convertCategoryResponseToEntity(categoryResponse, promotion);
    }

    private Category updateCategory(Category category, CategoryResponse categoryResponse, Promotion promotion) {
        return CategoryAdapter.preencherCategory(category, categoryResponse, promotion);
    }
}
