package com.carrefour.br.ecommerce.seller.vetx.promotions.repository;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.SkusGift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SkuGiftRepository extends JpaRepository<SkusGift, Long> {

    @Query(value = "SELECT sku.* " +
            " FROM vtex.promotion p " +
            "  LEFT JOIN vtex.sku_gift sku " +
            "    ON  p.id_sku_gift = sku.id" +
            "   WHERE sku.quantity_selectable = :quantitySelectable" +
            "    AND p.id = :promotion",
            nativeQuery = true)
    Optional<SkusGift> serchSkusGiftByPromotionAndGift(@Param("quantitySelectable") Integer quantitySelectable,
                                                       @Param("promotion") Long promotion);
}
