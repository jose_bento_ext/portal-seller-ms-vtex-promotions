package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.ProductSpecification;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class ProductSpecificationAdapter {

    public static ProductSpecification convertProductSpecificationToEntity(String productSpecificationString, Promotion promotion) {
        ProductSpecification giftListType = getEntity(new ProductSpecification(), productSpecificationString, promotion);
        giftListType.setActive(Boolean.TRUE);
        giftListType.setCreateDate(LocalDateTime.now());
        return giftListType;
    }

    public static ProductSpecification preencherProductSpecification(ProductSpecification giftListType, String productSpecificationString, Promotion promotion) {
        ProductSpecification newProductSpecification = getEntity(giftListType, productSpecificationString, promotion);
        newProductSpecification.setUpdateDate(LocalDateTime.now());
        return newProductSpecification;
    }

    private static ProductSpecification getEntity(ProductSpecification giftListType, String productSpecificationString, Promotion promotion) {
        giftListType.setSpecification(Optional.ofNullable(productSpecificationString).orElse(null));
        giftListType.setPromotion(promotion);
        return giftListType;
    }
}
