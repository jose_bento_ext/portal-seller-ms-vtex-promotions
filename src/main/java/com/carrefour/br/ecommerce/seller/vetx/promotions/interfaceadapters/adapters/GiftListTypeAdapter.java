package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.GiftListType;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class GiftListTypeAdapter {

    public static GiftListType convertGiftListTypeToEntity(String giftListTypeString, Promotion promotion) {
        GiftListType giftListType = getEntity(new GiftListType(), giftListTypeString, promotion);
        giftListType.setActive(Boolean.TRUE);
        giftListType.setCreateDate(LocalDateTime.now());
        return giftListType;
    }

    public static GiftListType preencherGiftListType(GiftListType giftListType, String giftListTypeString, Promotion promotion) {
        GiftListType newGiftListType = getEntity(giftListType, giftListTypeString, promotion);
        newGiftListType.setUpdateDate(LocalDateTime.now());
        return newGiftListType;
    }

    private static GiftListType getEntity(GiftListType giftListType, String giftListTypeString, Promotion promotion) {
        giftListType.setType(Optional.ofNullable(giftListTypeString).orElse(null));
        giftListType.setPromotion(promotion);
        return giftListType;
    }
}
