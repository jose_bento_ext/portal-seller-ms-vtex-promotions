package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Sku;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.SkuResponse;

import java.time.LocalDateTime;
import java.util.Optional;

public class SkuAdapter {

    public static Sku convertSkuResponseToEntity(SkuResponse skuResponse, Promotion promotion) {
        Sku sku = getEntity(new Sku(), skuResponse, promotion);
        sku.setActive(Boolean.TRUE);
        sku.setCreateDate(LocalDateTime.now());
        return sku;
    }

    public static Sku preencherSku(Sku sku, SkuResponse skuResponse, Promotion promotion) {
        Sku newSku = getEntity(sku, skuResponse, promotion);
        newSku.setUpdateDate(LocalDateTime.now());
        return newSku;
    }

    private static Sku getEntity(Sku sku, SkuResponse skuResponse, Promotion promotion) {
        sku.setIdVtex(Optional.ofNullable(skuResponse.getId()).orElse(null));
        sku.setName(Optional.ofNullable(skuResponse.getName()).orElse(null));
        sku.setPromotion(promotion);
        return sku;
    }
}
