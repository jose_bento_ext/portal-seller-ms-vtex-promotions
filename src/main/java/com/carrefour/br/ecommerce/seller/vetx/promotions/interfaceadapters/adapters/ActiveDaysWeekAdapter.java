package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.ActiveDaysOfWeek;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class ActiveDaysWeekAdapter {

    public static ActiveDaysOfWeek convertActiveDaysOfWeekToEntity(String daysOfWeek, Promotion promotion) {
        ActiveDaysOfWeek activeDaysOfWeek = getEntity(new ActiveDaysOfWeek(), daysOfWeek, promotion);
        activeDaysOfWeek.setActive(Boolean.TRUE);
        activeDaysOfWeek.setCreateDate(LocalDateTime.now());
        return activeDaysOfWeek;
    }

    public static ActiveDaysOfWeek preencherSkusGift(ActiveDaysOfWeek activeDaysOfWeek, String daysOfWeek, Promotion promotion) {
        ActiveDaysOfWeek newActiveDaysOfWeek = getEntity(activeDaysOfWeek, daysOfWeek, promotion);
        newActiveDaysOfWeek.setUpdateDate(LocalDateTime.now());
        return newActiveDaysOfWeek;
    }

    private static ActiveDaysOfWeek getEntity(ActiveDaysOfWeek activeDaysOfWeek, String daysOfWeek, Promotion promotion) {
        activeDaysOfWeek.setActiveDaysOfWeek(Optional.ofNullable(daysOfWeek).orElse(null));
        activeDaysOfWeek.setPromotion(promotion);
        return activeDaysOfWeek;
    }
}
