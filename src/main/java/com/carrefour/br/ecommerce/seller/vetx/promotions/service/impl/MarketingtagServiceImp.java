package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.MarketingTag;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.MarketingTagAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.MarketingTagRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.MarketingtagService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MarketingtagServiceImp implements MarketingtagService {

    @Autowired
    private MarketingTagRepository marketingTagRepository;

    private void saveMarketingTag(MarketingTag marketingTag) {
        marketingTagRepository.saveAndFlush(marketingTag);
    }

    @Override
    public void fillAndSaveEntity(List<String> marketingtag, Promotion promotion) {
        String marketing = marketingtag.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<MarketingTag> marketingTagOptional = marketingTagRepository.findByPromotionAndMarketingTag(promotion, marketing);
            if (marketingTagOptional.isPresent()) {
                saveMarketingTag(MarketingTagAdapter.preencherSaleChannelPromotion(marketingTagOptional.get(), marketing, promotion));
                return;
            }
        }
        saveMarketingTag(MarketingTagAdapter.convertSaleChannelPromotionToEntity(marketing, promotion));
    }
}
