package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.ActiveDaysOfWeek;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.ActiveDaysWeekAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.ActiveDaysOfWeekRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.ActiveDaysOfWeekService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ActiveDaysOfWeekServiceImp implements ActiveDaysOfWeekService {

    @Autowired
    private ActiveDaysOfWeekRepository activeDaysOfWeekRepository;

    private void saveActiveDaysOfWeek(ActiveDaysOfWeek activeDaysOfWeek) {
        activeDaysOfWeekRepository.saveAndFlush(activeDaysOfWeek);
    }

    @Override
    public void fillAndSaveEntity(List<String> activeDayOfWeek, Promotion promotion) {
        String activeDay = activeDayOfWeek.toString().replaceAll("[\\[\\](){}]","");
        if(Objects.nonNull(promotion)){
            Optional<ActiveDaysOfWeek> activeDaysOfWeekOptional = activeDaysOfWeekRepository.findByPromotionAndActiveDaysOfWeek(promotion, activeDay);
            if (activeDaysOfWeekOptional.isPresent()) {
                ActiveDaysOfWeek activeDaysOfWeek = ActiveDaysWeekAdapter.preencherSkusGift(activeDaysOfWeekOptional.get(), activeDay, promotion);
                saveActiveDaysOfWeek(activeDaysOfWeek);
                return ;
            }
        }
        ActiveDaysOfWeek newActiveDaysOfWeek = ActiveDaysWeekAdapter.convertActiveDaysOfWeekToEntity(activeDay, promotion);
        saveActiveDaysOfWeek(newActiveDaysOfWeek);
    }
}
