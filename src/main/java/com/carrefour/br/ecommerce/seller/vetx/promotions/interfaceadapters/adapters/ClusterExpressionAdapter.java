package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.ClusterExpression;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class ClusterExpressionAdapter {

    public static ClusterExpression convertClusterExpressionToEntity(String clusterExpressionString, Promotion promotion) {
        ClusterExpression clusterExpression = getEntity(new ClusterExpression(), clusterExpressionString, promotion);
        clusterExpression.setActive(Boolean.TRUE);
        clusterExpression.setCreateDate(LocalDateTime.now());
        return clusterExpression;
    }

    public static ClusterExpression preencherClusterExpression(ClusterExpression clusterExpression, String clusterExpressionString, Promotion promotion) {
        ClusterExpression newClusterExpression = getEntity(clusterExpression, clusterExpressionString, promotion);
        newClusterExpression.setUpdateDate(LocalDateTime.now());
        return newClusterExpression;
    }

    private static ClusterExpression getEntity(ClusterExpression clusterExpression, String clusterExpressionString, Promotion promotion) {
        clusterExpression.setClusterExpression(Optional.ofNullable(clusterExpressionString).orElse(null));
        clusterExpression.setPromotion(promotion);
        return clusterExpression;
    }
}
