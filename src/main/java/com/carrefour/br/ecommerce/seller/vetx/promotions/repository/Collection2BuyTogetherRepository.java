package com.carrefour.br.ecommerce.seller.vetx.promotions.repository;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Collection2BuyTogether;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface Collection2BuyTogetherRepository extends JpaRepository<Collection2BuyTogether, Long> {
    Optional<Collection2BuyTogether> findByPromotionAndCollectionBuyTogether(Promotion promotion, String store);
}
