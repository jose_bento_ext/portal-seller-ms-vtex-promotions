package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.ClusterExpression;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.ClusterExpressionAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.ClusterExpressionRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.ClusterExpressionService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ClusterExpressionServiceImp implements ClusterExpressionService {

    @Autowired
    private ClusterExpressionRepository clusterExpressionRepository;

    private void saveClusterExpression(ClusterExpression merchant) {
        clusterExpressionRepository.saveAndFlush(merchant);
    }

    @Override
    public void fillAndSaveEntity(List<String> clusterExpressions, Promotion promotion) {
        String clusterExpressionsString = clusterExpressions.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<ClusterExpression> clusterExpressionOptional = clusterExpressionRepository.findByPromotionAndClusterExpression(promotion, clusterExpressionsString);
            if (clusterExpressionOptional.isPresent()) {
                ClusterExpression clusterExpression = ClusterExpressionAdapter.preencherClusterExpression(clusterExpressionOptional.get(), clusterExpressionsString, promotion);
                saveClusterExpression(clusterExpression);
                return;
            }
        }
        ClusterExpression newClusterExpression = ClusterExpressionAdapter.convertClusterExpressionToEntity(clusterExpressionsString, promotion);
        saveClusterExpression(newClusterExpression);
    }
}
