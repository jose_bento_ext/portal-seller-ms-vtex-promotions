package com.carrefour.br.ecommerce.seller.vetx.promotions.repository;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.MarketingTag;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MarketingTagRepository extends JpaRepository<MarketingTag, Long> {
    Optional<MarketingTag> findByPromotionAndMarketingTag(Promotion promotion, String marketingTag);
}
