package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.ZipCode;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.ZipCodeAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.ZipCodeResponse;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.ZipCodeRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.ZipCodeService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ZipCodeServiceImp implements ZipCodeService {

    @Autowired
    private ZipCodeRepository zipCodeRepository;

    private void saveSku(ZipCode zipCode) {
        zipCodeRepository.saveAndFlush(zipCode);
    }

    @Override
    public void fillAndSaveEntity(List<ZipCodeResponse> zipCodeResponses, Promotion promotion) {
        if (Objects.nonNull(promotion)) {
            zipCodeResponses.forEach(zipCodeResponse -> {
                Optional<ZipCode> zipCodeOptional = zipCodeRepository.findByPromotionAndZipCodeFromAndZipCodeToAndInclusive(promotion, zipCodeResponse.getZipCodeFrom(), zipCodeResponse.getZipCodeTo(), zipCodeResponse.getInclusive());
                ZipCode zipCode = zipCodeOptional.isPresent() ? updateZipCode(zipCodeOptional.get(), zipCodeResponse, promotion)
                        : newZipCode(zipCodeResponse, promotion);
                saveSku(zipCode);
            });
        }
    }

    private ZipCode newZipCode(ZipCodeResponse zipCodeResponse, Promotion promotion) {
        return ZipCodeAdapter.convertZipCodeResponseToEntity(zipCodeResponse, promotion);
    }

    private ZipCode updateZipCode(ZipCode sku, ZipCodeResponse zipCodeResponse, Promotion promotion) {
        return ZipCodeAdapter.preencherZipCode(sku, zipCodeResponse, promotion);
    }
}
