package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.PaymentRule;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.PaymentRuleAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.PaymentRuleRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.PaymentRuleService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PaymentRuleServiceImp implements PaymentRuleService {

    @Autowired
    private PaymentRuleRepository paymentRuleRepository;

    private void savePaymentRule(PaymentRule merchant) {
        paymentRuleRepository.saveAndFlush(merchant);
    }

    @Override
    public void fillAndSaveEntity(List<String> paymentRules, Promotion promotion) {
        String paymentRuleString = paymentRules.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<PaymentRule> paymentRuleOptional = paymentRuleRepository.findByPromotionAndPaymentsRule(promotion, paymentRuleString);
            if (paymentRuleOptional.isPresent()) {
                PaymentRule paymentRule = PaymentRuleAdapter.preencherPaymentRule(paymentRuleOptional.get(), paymentRuleString, promotion);
                savePaymentRule(paymentRule);
                return;
            }
        }
        PaymentRule newPaymentRule = PaymentRuleAdapter.convertPaymentRuleToEntity(paymentRuleString, promotion);
        savePaymentRule(newPaymentRule);
    }
}
