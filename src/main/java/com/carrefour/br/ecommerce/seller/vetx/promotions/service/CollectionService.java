package com.carrefour.br.ecommerce.seller.vetx.promotions.service;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion.CollectionResponse;

import java.util.List;

public interface CollectionService {
    void fillAndSaveEntity(List<CollectionResponse> collectionResponses, Promotion promotion);
}
