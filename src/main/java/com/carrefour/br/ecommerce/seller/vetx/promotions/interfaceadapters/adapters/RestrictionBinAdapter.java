package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.RestrictionBin;

import java.time.LocalDateTime;
import java.util.Optional;

public class RestrictionBinAdapter {

    public static RestrictionBin convertRestrictionBinToEntity(String restrictionBinString, Promotion promotion) {
        RestrictionBin restrictionBin = getEntity(new RestrictionBin(), restrictionBinString, promotion);
        restrictionBin.setActive(Boolean.TRUE);
        restrictionBin.setCreateDate(LocalDateTime.now());
        return restrictionBin;
    }

    public static RestrictionBin preencherRestrictionBin(RestrictionBin restrictionBin, String restrictionBinString, Promotion promotion) {
        RestrictionBin newRestrictionBin = getEntity(restrictionBin, restrictionBinString, promotion);
        newRestrictionBin.setUpdateDate(LocalDateTime.now());
        return newRestrictionBin;
    }

    private static RestrictionBin getEntity(RestrictionBin restrictionBin, String restrictionBinString, Promotion promotion) {
        restrictionBin.setRestrictionBin(Optional.ofNullable(restrictionBinString).orElse(null));
        restrictionBin.setPromotion(promotion);
        return restrictionBin;
    }
}
