package com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SkuGiftResponse {

    @JsonProperty("quantitySelectable")
    private Integer quantitySelectable;

    @JsonProperty("gifts")
    private List<String> gifts;

    public String getGifts() {
        return new Gson().toJson(gifts);
    }
}
