package com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.PaymentRule;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;

import java.time.LocalDateTime;
import java.util.Optional;

public class PaymentRuleAdapter {

    public static PaymentRule convertPaymentRuleToEntity(String clusterExpressionString, Promotion promotion) {
        PaymentRule paymentRule = getEntity(new PaymentRule(), clusterExpressionString, promotion);
        paymentRule.setActive(Boolean.TRUE);
        paymentRule.setCreateDate(LocalDateTime.now());
        return paymentRule;
    }

    public static PaymentRule preencherPaymentRule(PaymentRule paymentRule, String clusterExpressionString, Promotion promotion) {
        PaymentRule newPaymentRule = getEntity(paymentRule, clusterExpressionString, promotion);
        newPaymentRule.setUpdateDate(LocalDateTime.now());
        return newPaymentRule;
    }

    private static PaymentRule getEntity(PaymentRule paymentRule, String clusterExpressionString, Promotion promotion) {
        paymentRule.setPaymentsRule(Optional.ofNullable(clusterExpressionString).orElse(null));
        paymentRule.setPromotion(promotion);
        return paymentRule;
    }
}
