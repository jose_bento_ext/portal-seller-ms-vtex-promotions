package com.carrefour.br.ecommerce.seller.vetx.promotions.exception;

public class IntegrationException extends Exception {

    public IntegrationException() {
        super();
    }

    public IntegrationException(String message) {
        super(message);
    }

    public IntegrationException(String message, Throwable cause) {
        super(message, cause);
    }
}
