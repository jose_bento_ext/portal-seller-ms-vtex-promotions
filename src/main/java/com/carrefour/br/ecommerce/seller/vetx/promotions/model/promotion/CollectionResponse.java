package com.carrefour.br.ecommerce.seller.vetx.promotions.model.promotion;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CollectionResponse {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

}
