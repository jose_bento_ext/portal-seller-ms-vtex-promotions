package com.carrefour.br.ecommerce.seller.vetx.promotions.service.impl;

import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Coupon;
import com.carrefour.br.ecommerce.seller.vetx.promotions.entity.promotion.Promotion;
import com.carrefour.br.ecommerce.seller.vetx.promotions.interfaceadapters.adapters.CouponAdapter;
import com.carrefour.br.ecommerce.seller.vetx.promotions.repository.CouponRepository;
import com.carrefour.br.ecommerce.seller.vetx.promotions.service.CouponService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CouponsServiceImp implements CouponService {

    @Autowired
    private CouponRepository couponRepository;

    private void saveCoupom(Coupon store) {
        couponRepository.saveAndFlush(store);
    }

    @Override
    public void fillAndSaveEntity(List<String> coupons, Promotion promotion) {
        String couponString = coupons.toString().replaceAll("[\\[\\](){}]", "");
        if (Objects.nonNull(promotion)) {
            Optional<Coupon> couponOptional = couponRepository.findByPromotionAndCoupon(promotion, couponString);
            if (couponOptional.isPresent()) {
                Coupon coupon = CouponAdapter.preencherCoupon(couponOptional.get(), couponString, promotion);
                saveCoupom(coupon);
                return;
            }
        }
        Coupon newCoupon = CouponAdapter.convertCouponToEntity(couponString, promotion);
        saveCoupom(newCoupon);
    }
}
