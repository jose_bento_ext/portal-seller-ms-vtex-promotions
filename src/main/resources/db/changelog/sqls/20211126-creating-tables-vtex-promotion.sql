CREATE SCHEMA vtex;

CREATE TABLE IF NOT EXISTS vtex.sku_gift
(
    id                  bigint not null,
    quantity_selectable integer,
    gift                jsonb,
    active              boolean,
    create_date         timestamp not null,
    update_date         timestamp,

    CONSTRAINT sku_gift_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE IF NOT EXISTS vtex.sku_gift_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.sku_gift_id_seq OWNER TO postgres;
ALTER TABLE vtex.sku_gift ALTER COLUMN id SET DEFAULT nextval('vtex.sku_gift_id_seq'::regclass);
ALTER TABLE vtex.sku_gift OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.promotion
(
    id                                             bigint not null,
    id_calculator_configuration                    varchar(255),
    name_promotion                                 varchar(255),
    description                                    varchar(255),
    begin_date_utc                                 timestamp,
    end_date_utc                                   timestamp,
    last_modified                                  timestamp,
    days_ago_of_purchases                          integer,
    is_active                                      boolean,
    is_archived                                    boolean,
    is_featured                                    boolean,
    disable_deal                                   boolean,
    offset_promotion                               integer,
    activate_gifts_multiplier                      boolean,
    new_offset                                     double precision,
    cumulative                                     boolean,
    effect_type                                    varchar(255),
    discount_type                                  varchar(255),
    nominal_shipping_discount_value                double precision,
    absolute_shipping_discount_value               double precision,
    nominal_discount_value                         double precision,
    maximum_unitprice_discount                     double precision,
    percentual_discount_value                      double precision,
    rebate_percentual_discount_value               double precision,
    percentual_shipping_discount_value             double precision,
    percentual_tax                                 double precision,
    shipping_percentual_tax                        double precision,
    percentual_discount_value_list1                double precision,
    percentual_discount_value_list2                double precision,
    id_sku_gift                                    bigint,
    nominal_reward_value                           double precision,
    percentual_reward_value                        double precision,
    order_status_reward_value                      varchar(255),
    max_number_of_affected_items                   integer,
    max_number_of_affected_items_group_key         varchar(255),
    apply_to_all_shippings                         boolean,
    nominal_tax                                    double precision,
    origin                                         varchar(255),
    id_seller                                      varchar(255),
    id_seller_is_inclusive                         boolean,
    are_sales_channel_ids_exclusive                boolean,
    marketing_tags_are_not_inclusive               boolean,
    stores_are_inclusive                           boolean,
    categories_are_inclusive                       boolean,
    brands_are_inclusive                           boolean,
    products_are_inclusive                         boolean,
    skus_are_inclusive                             boolean,
    utm_source                                     varchar(255),
    utm_campaign                                   varchar(255),
    minimum_quantity_buy_together                  integer,
    quantity_to_affect_buy_together                integer,
    enable_buy_together_per_sku                    boolean,
    total_value_floor                              double precision,
    total_value_celing                             double precision,
    total_value_include_all_items                  boolean,
    total_value_mode                               varchar(255),
    collections_is_inclusive                       boolean,
    total_value_purchase                           double precision,
    is_sla_selected                                boolean,
    is_first_buy                                   boolean,
    first_buy_is_profile_optimistic                boolean,
    compare_list_price_and_price                   boolean,
    is_different_list_price_and_price              boolean,
    item_max_price                                 double precision,
    item_min_price                                 double precision,
    installment                                    integer,
    is_min_max_installments                        boolean,
    min_installment                                integer,
    max_installment                                integer,
    max_usage                                      integer,
    max_usage_per_client                           integer,
    should_distribute_discount_among_matched_items boolean,
    multiple_use_per_client                        boolean,
    accumulate_with_manual_price                        boolean,
    type_promotion                                 varchar(255),
    use_new_progressive_algorithm                  boolean,
    active                                         boolean,
    create_date                                    timestamp not null,
    update_date                                    timestamp,

    CONSTRAINT promotion_pkey PRIMARY KEY (id),
    CONSTRAINT fk_promotion_skus_gift FOREIGN KEY (id_sku_gift)
    REFERENCES vtex.sku_gift (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE SEQUENCE IF NOT EXISTS vtex.promotion_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.promotion_id_seq OWNER TO postgres;
ALTER TABLE vtex.promotion ALTER COLUMN id SET DEFAULT nextval('vtex.promotion_id_seq'::regclass);
ALTER TABLE vtex.promotion OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.payment_method
(
    id                  bigint not null,
    id_vtex             varchar(255),
    name_payment        varchar(255),
    active              boolean,
    create_date         timestamp not null,
    update_date         timestamp,
    id_promotion            bigint,

    CONSTRAINT payment_method_pkey PRIMARY KEY (id),
    CONSTRAINT fk_payment_method_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.payment_method_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.payment_method_id_seq OWNER TO postgres;
ALTER TABLE vtex.payment_method ALTER COLUMN id SET DEFAULT nextval('vtex.payment_method_id_seq'::regclass);
ALTER TABLE vtex.payment_method OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.zip_code
(
    id                      bigint not null,
    zip_code_from           varchar(255),
    zip_code_to             varchar(255),
    inclusive               boolean,
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT zip_code_pkey PRIMARY KEY (id),
    CONSTRAINT fk_zip_code_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.zip_code_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.zip_code OWNER TO postgres;
ALTER TABLE vtex.zip_code ALTER COLUMN id SET DEFAULT nextval('vtex.zip_code_id_seq'::regclass);
ALTER TABLE vtex.zip_code OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.percentual_discount_value
(
    id                      bigint not null,
    percentual              varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT percentual_discount_value_pkey PRIMARY KEY (id),
    CONSTRAINT fk_percentual_discount_value_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.percentual_discount_value_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.percentual_discount_value OWNER TO postgres;
ALTER TABLE vtex.percentual_discount_value ALTER COLUMN id SET DEFAULT nextval('vtex.percentual_discount_value_id_seq'::regclass);
ALTER TABLE vtex.percentual_discount_value OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.affiliate
(
    id                      bigint not null,
    id_vtex                 varchar(255),
    name_affiliate          varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT affiliate_pkey PRIMARY KEY (id),
    CONSTRAINT fk_affiliate_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.affiliate_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.affiliate_id_seq OWNER TO postgres;
ALTER TABLE vtex.affiliate ALTER COLUMN id SET DEFAULT nextval('vtex.affiliate_id_seq'::regclass);
ALTER TABLE vtex.affiliate OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.product_specification
(
    id                      bigint not null,
    specification           varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT product_specification_pkey PRIMARY KEY (id),
    CONSTRAINT fk_product_specification_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.product_specification_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.product_specification_id_seq OWNER TO postgres;
ALTER TABLE vtex.product_specification ALTER COLUMN id SET DEFAULT nextval('vtex.product_specification_id_seq'::regclass);
ALTER TABLE vtex.product_specification OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.gift_list_type
(
    id                      bigint not null,
    type_gift               varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT gift_list_type_pkey PRIMARY KEY (id),
    CONSTRAINT fk_gift_list_type_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.gift_list_type_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.gift_list_type_id_seq OWNER TO postgres;
ALTER TABLE vtex.gift_list_type ALTER COLUMN id SET DEFAULT nextval('vtex.gift_list_type_id_seq'::regclass);
ALTER TABLE vtex.gift_list_type OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.payment_rule
(
    id                      bigint not null,
    payment_rule            varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT payment_rule_pkey PRIMARY KEY (id),
    CONSTRAINT fk_payment_rule_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.payment_rule_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.payment_rule_id_seq OWNER TO postgres;
ALTER TABLE vtex.payment_rule ALTER COLUMN id SET DEFAULT nextval('vtex.payment_rule_id_seq'::regclass);
ALTER TABLE vtex.payment_rule OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.cluster_expression
(
    id                      bigint not null,
    cluster_expression      TEXT,
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT cluster_expression_pkey PRIMARY KEY (id),
    CONSTRAINT fk_cluster_expression_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.cluster_expression_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.cluster_expression_id_seq OWNER TO postgres;
ALTER TABLE vtex.cluster_expression ALTER COLUMN id SET DEFAULT nextval('vtex.cluster_expression_id_seq'::regclass);
ALTER TABLE vtex.cluster_expression OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.merchant
(
    id                      bigint not null,
    merchant                varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT merchant_pkey PRIMARY KEY (id),
    CONSTRAINT fk_merchant_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.merchant_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.merchant_id_seq OWNER TO postgres;
ALTER TABLE vtex.merchant ALTER COLUMN id SET DEFAULT nextval('vtex.merchant_id_seq'::regclass);
ALTER TABLE vtex.merchant OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.sla_promotion
(
    id                      bigint not null,
    sla_id                  varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT sla_promotion_pkey PRIMARY KEY (id),
    CONSTRAINT fk_sla_promotion_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.sla_promotion_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.sla_promotion_id_seq OWNER TO postgres;
ALTER TABLE vtex.sla_promotion ALTER COLUMN id SET DEFAULT nextval('vtex.sla_promotion_id_seq'::regclass);
ALTER TABLE vtex.sla_promotion OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.card_issuer
(
    id                      bigint not null,
    card_issuer             varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT card_issuer_pkey PRIMARY KEY (id),
    CONSTRAINT fk_card_issuer_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.card_issuer_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.card_issuer_id_seq OWNER TO postgres;
ALTER TABLE vtex.card_issuer ALTER COLUMN id SET DEFAULT nextval('vtex.card_issuer_id_seq'::regclass);
ALTER TABLE vtex.card_issuer OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.restriction_bin
(
    id                      bigint not null,
    restriction_bin         varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT restriction_bin_pkey PRIMARY KEY (id),
    CONSTRAINT fk_restriction_bin_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.restriction_bin_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.restriction_bin_id_seq OWNER TO postgres;
ALTER TABLE vtex.restriction_bin ALTER COLUMN id SET DEFAULT nextval('vtex.restriction_bin_id_seq'::regclass);
ALTER TABLE vtex.restriction_bin OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.collection
(
    id                      bigint not null,
    id_vtex                 varchar(255),
    name_collection         varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT collection_pkey PRIMARY KEY (id),
    CONSTRAINT fk_coupon_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.collection_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.collection_id_seq OWNER TO postgres;
ALTER TABLE vtex.collection ALTER COLUMN id SET DEFAULT nextval('vtex.collection_id_seq'::regclass);
ALTER TABLE vtex.collection OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.coupon
(
    id                      bigint not null,
    coupon                  varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT coupon_pkey PRIMARY KEY (id),
    CONSTRAINT fk_coupon_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.coupon_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.coupon_id_seq OWNER TO postgres;
ALTER TABLE vtex.coupon ALTER COLUMN id SET DEFAULT nextval('vtex.coupon_id_seq'::regclass);
ALTER TABLE vtex.coupon OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.sku2_buy_together
(
    id                      bigint not null,
    sku_buy_together        varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT sku2_buy_together_pkey PRIMARY KEY (id),
    CONSTRAINT fk_sku2_buy_together_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.sku2_buy_together_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.sku2_buy_together_id_seq OWNER TO postgres;
ALTER TABLE vtex.sku2_buy_together ALTER COLUMN id SET DEFAULT nextval('vtex.sku2_buy_together_id_seq'::regclass);
ALTER TABLE vtex.sku2_buy_together OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.sku1_buy_together
(
    id                      bigint not null,
    sku_buy_together        varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT sku1_buy_together_pkey PRIMARY KEY (id),
    CONSTRAINT fk_sku1_buy_together_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.sku1_buy_together_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.sku1_buy_together_id_seq OWNER TO postgres;
ALTER TABLE vtex.sku1_buy_together ALTER COLUMN id SET DEFAULT nextval('vtex.sku1_buy_together_id_seq'::regclass);
ALTER TABLE vtex.sku1_buy_together OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.collection2_buy_together
(
    id                      bigint not null,
    collection_buy_together varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT collection2_buy_together_pkey PRIMARY KEY (id),
    CONSTRAINT fk_collection2_buy_together_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.collection2_buy_together_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.collection2_buy_together_id_seq OWNER TO postgres;
ALTER TABLE vtex.collection2_buy_together ALTER COLUMN id SET DEFAULT nextval('vtex.collection2_buy_together_id_seq'::regclass);
ALTER TABLE vtex.collection2_buy_together OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.collection1_buy_together
(
    id                      bigint not null,
    collection_buy_together varchar(255),
    active                  boolean,
    create_date             timestamp not null,
    update_date             timestamp,
    id_promotion            bigint,

    CONSTRAINT collection1_buy_together_pkey PRIMARY KEY (id),
    CONSTRAINT fk_collection1_buy_together_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.collection1_buy_together_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.collection1_buy_together_id_seq OWNER TO postgres;
ALTER TABLE vtex.collection1_buy_together ALTER COLUMN id SET DEFAULT nextval('vtex.collection1_buy_together_id_seq'::regclass);
ALTER TABLE vtex.collection1_buy_together OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.sku
(
    id              bigint not null,
    id_vtex         varchar(255),
    name_sku        varchar(255),
    active          boolean,
    create_date     timestamp not null,
    update_date     timestamp,
    id_promotion    bigint,

    CONSTRAINT sku_pkey PRIMARY KEY (id),
    CONSTRAINT fk_sku_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.sku_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.sku_id_seq OWNER TO postgres;
ALTER TABLE vtex.sku ALTER COLUMN id SET DEFAULT nextval('vtex.sku_id_seq'::regclass);
ALTER TABLE vtex.sku OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.product
(
    id              bigint not null,
    id_vtex         varchar(255),
    name_product    varchar(255),
    active          boolean,
    create_date     timestamp not null,
    update_date     timestamp,
    id_promotion     bigint,

    CONSTRAINT product_pkey PRIMARY KEY (id),
    CONSTRAINT fk_product_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.product_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.product_id_seq OWNER TO postgres;
ALTER TABLE vtex.product ALTER COLUMN id SET DEFAULT nextval('vtex.product_id_seq'::regclass);
ALTER TABLE vtex.product OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.brand
(
    id             bigint not null,
    id_vtex        varchar(255),
    name_brand     varchar(255),
    active         boolean,
    create_date    timestamp not null,
    update_date    timestamp,
    id_promotion   bigint,

    CONSTRAINT brand_pkey PRIMARY KEY (id),
    CONSTRAINT fk_brand_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.brand_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.brand_id_seq OWNER TO postgres;
ALTER TABLE vtex.brand ALTER COLUMN id SET DEFAULT nextval('vtex.brand_id_seq'::regclass);
ALTER TABLE vtex.brand OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.category
(
    id             bigint not null,
    id_vtex        varchar(255),
    name_category  varchar(255),
    active         boolean,
    create_date    timestamp not null,
    update_date    timestamp,
    id_promotion   bigint,

    CONSTRAINT category_pkey PRIMARY KEY (id),
    CONSTRAINT fk_category_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.category_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.category_id_seq OWNER TO postgres;
ALTER TABLE vtex.category ALTER COLUMN id SET DEFAULT nextval('vtex.category_id_seq'::regclass);
ALTER TABLE vtex.category OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.campaign_promotion
(
    id           bigint not null,
    campaign     varchar(255),
    active       boolean,
    create_date  timestamp not null,
    update_date  timestamp,
    id_promotion bigint,

    CONSTRAINT campaign_promotion_pkey PRIMARY KEY (id),
    CONSTRAINT fk_campaign_promotion_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.campaign_promotion_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.campaign_promotion_id_seq OWNER TO postgres;
ALTER TABLE vtex.campaign_promotion ALTER COLUMN id SET DEFAULT nextval('vtex.campaign_promotion_id_seq'::regclass);
ALTER TABLE vtex.campaign_promotion OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.store
(
    id           bigint not null,
    store        varchar(255),
    active       boolean,
    create_date  timestamp not null,
    update_date  timestamp,
    id_promotion bigint,

    CONSTRAINT store_pkey PRIMARY KEY (id),
    CONSTRAINT fk_store_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.store_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.store_id_seq OWNER TO postgres;
ALTER TABLE vtex.store ALTER COLUMN id SET DEFAULT nextval('vtex.store_id_seq'::regclass);
ALTER TABLE vtex.store OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.marketing_tag
(
    id            bigint not null,
    marketing_tag varchar(255),
    active        boolean,
    create_date   timestamp not null,
    update_date   timestamp,
    id_promotion  bigint,

    CONSTRAINT marketing_tag_pkey PRIMARY KEY (id),
    CONSTRAINT fk_marketing_tag_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.marketing_tag_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.marketing_tag_id_seq OWNER TO postgres;
ALTER TABLE vtex.marketing_tag ALTER COLUMN id SET DEFAULT nextval('vtex.marketing_tag_id_seq'::regclass);
ALTER TABLE vtex.marketing_tag OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.sale_channel_promotion
(
    id            bigint not null,
    sales_channel varchar(255),
    active        boolean,
    create_date   timestamp not null,
    update_date   timestamp,
    id_promotion  bigint,

    CONSTRAINT sale_channel_promotion_pkey PRIMARY KEY (id),
    CONSTRAINT fk_sale_channel_promotion_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.sale_channel_promotion_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.sale_channel_promotion_id_seq OWNER TO postgres;
ALTER TABLE vtex.sale_channel_promotion ALTER COLUMN id SET DEFAULT nextval('vtex.sale_channel_promotion_id_seq'::regclass);
ALTER TABLE vtex.sale_channel_promotion OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.max_price_per_item
(
    id                 bigint not null,
    max_price_per_item varchar(255),
    active             boolean,
    create_date        timestamp not null,
    update_date        timestamp,
    id_promotion       bigint,

    CONSTRAINT max_price_per_item_pkey PRIMARY KEY (id),
    CONSTRAINT fk_max_price_per_item_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
    );

CREATE SEQUENCE IF NOT EXISTS vtex.max_price_per_item_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.max_price_per_item_id_seq OWNER TO postgres;
ALTER TABLE vtex.max_price_per_item ALTER COLUMN id SET DEFAULT nextval('vtex.max_price_per_item_id_seq'::regclass);
ALTER TABLE vtex.max_price_per_item OWNER TO POSTGRES;

CREATE TABLE IF NOT EXISTS vtex.active_day_of_week
(
    id           bigint not null,
    days_of_week varchar(255),
    active       boolean,
    create_date  timestamp not null,
    update_date  timestamp,
    id_promotion bigint,

    CONSTRAINT active_day_of_week_pkey PRIMARY KEY (id),
    CONSTRAINT fk_active_day_of_week_promotion FOREIGN KEY (id_promotion)
    REFERENCES vtex.promotion (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE SEQUENCE IF NOT EXISTS vtex.active_day_of_week_id_seq
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

ALTER TABLE vtex.active_day_of_week_id_seq OWNER TO postgres;
ALTER TABLE vtex.active_day_of_week ALTER COLUMN id SET DEFAULT nextval('vtex.active_day_of_week_id_seq'::regclass);
ALTER TABLE vtex.active_day_of_week OWNER TO POSTGRES;